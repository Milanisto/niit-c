/*
 Программа, которая принимает строку от пользователя и
выводит её на экран, выравнивая по центру.  вторая версия 
by Wilson Castro. 10.2016
 */

#include<stdio.h>
#include<string.h>
#define ZISE 80 // длина строки на экране

int main()
 {
    char text[ZISE];
    char format[ZISE];
    int t, m;
    printf("Введите текст\n");
    fgets(text, ZISE, stdin);
    t = strlen(text);
    m = (ZISE - t) / 2;  // расчет места первой буквы на экране 
     sprintf(format,"%c%ds",'%', m );
    printf(format, text);
    return 0;
}