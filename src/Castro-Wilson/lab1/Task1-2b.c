/*Программа , которая запрашивает текущее время в формате ЧЧ:ММ:СС
а затем выводит приветствие
 Кастро Вилсон. 10.30.2016
 */

#include <stdio.h>
#include <string.h>

int main(void) {
    int hour = 0;
    int min = 0;
    int sec = 0;
    char ch;
    printf("Введите время в формате ЧЧ:ММ:СС\n");
    while (scanf("%d:%d:%d", &hour, &min, &sec) != 3) {// Функция для возможности ввода снова время в случае ошибки
        while ((ch = getchar()) != '\n') {
            puts("Вы не правильно ввели время в формате - ЧЧ:ММ:СС, введите его снова\n");
            scanf("%d:%d:%d", &hour, &min, &sec);
        }
    }

    if (hour >= 5 && hour < 12) {
        puts("Доброе утро!");
    }
    if (hour >= 12 && hour < 16) {
        puts("Добрый день!");
    }
    if (hour >= 16 && hour <= 23 || hour >= 00 && hour < 5)
        puts("Добрый вечер!");

    return 0;
}
