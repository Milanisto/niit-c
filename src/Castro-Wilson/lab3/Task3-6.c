/********************************************************************
 * Программа, которая формирует целочисленный массив размера N,     *
 * а затем находит сумму элементов между минимальным и максимальным *
 * эле ментами.                                                     *
 * By Wilson Castro. 12.2016.                                       *
 ********************************************************************/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define SIZE 256

int main() {
    int line[SIZE] = {0};
    int num = 0;
    int N = 0;
    int min = 0;
    int max = 0;
    int indexMin = 0;
    int indexMax = 0;
    int summa=0;
    srand(time(0));
    printf("Please, type the long of massive\n");
    scanf("%d", &N);
    for (int i = 0; i < N; i++) {
        num = rand() % 100;
        line[i] = num;
    }
    for (int s = 0; s < N; s++) {
        printf("%d, ", line[s]);
    }
    printf("\n");
    min = line[0];
    max = line[0];
    for (int t = 0; t < N; t++) {
        if (line[t] <= min) {
            indexMin = t;
            min = line[t];
        }
        if (line[t] >= max) {
            indexMax = t;
            max = line[t];
        }
    }
    printf("The lasted numbre is: %d\n", min);
    printf("The highest number is: %d\n", max);
    
    if(indexMin<indexMax){
        for(int i=indexMin+1; i<=indexMax-1; i++){
            summa+=line[i];
        }
    }
        else{
            for(int i=indexMax+1; i<=indexMin-1; i++){
            summa+=line[i];
        }
    }
     printf("The summa of elements of the intervel is: %d\n", summa);
    return 0;
}