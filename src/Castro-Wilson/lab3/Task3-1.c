/**********************************************************
 * Программа, подсчитывающую количество слов во введенной *
 * пользователем строке.                                  *
 * By Wilson Castro. 11.2016.                             *
 **********************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256

int main() {
    char line[SIZE] = {0};
    int i, j;
    int cuonter = 0;
    printf("Введите струку для обработки\n");
    fgets(line, 256, stdin);
    j = strlen(line);
    line[j - 1] = '\0';

    for (i = 1; i < j; i++) {
        if ( line[i-1] != ' ' && line[i] == ' ') { //цикл для случая окончаяния слова с пробелом 
            cuonter++;
        } 
      if (line[i-1] != ' '  && line[i] == '\0' ) { //цикл для случая окончаяния слова и строка 
            cuonter++;
        }
        if (line[i - 1] != ' ' && line[i] == '\n') { //цикл для случая окончаяния слова без пробела  
            cuonter++;
        } 
    }
    printf("Words: %d\n", cuonter);
    return 0;
}
