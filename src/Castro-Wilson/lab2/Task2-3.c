/*
 * Программа, выводящая на экран треугольник из звездочек.
 * by Wilson Castro. 11.2016 
 */

#include <stdio.h>
#define ZISE 80

int main() {
    int i, j, n;
    printf("Введите количество строк\n");
    scanf("%d", &n);
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= n - i; j++) {
            printf(" ");
        }
        for (j = 1; j <= 2 * i - 1; j++) {
            printf("*");
        }
        printf("\n");
    }
    return 0;
}

