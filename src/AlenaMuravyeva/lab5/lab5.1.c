/*
 ============================================================================
 Name        : lab5.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program shuffles the words in random order
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define ARR_SIZE 80
#define MAX_POINTER 40
#define MAX_GENERATE 21

enum Boolean{false,true};
typedef enum Boolean BOOL;
typedef struct RandPair RandPair;
int len=0;
struct RandPair
{
   unsigned int randNumber1;
   unsigned int randNumber2;
};

int counterWords=0;
char userString [ARR_SIZE]={0};
char *ptrArray[MAX_POINTER];


void getWords()
{
   int firstLetterIndex=0;
   int secondLetterIndex=0;
   int firstFound=0;
   int secondFound =0;

   len=strlen(userString);
   userString [len-2]='\0';
   len=len-2;

   for(int i=0;i<len;i++)
   {
       if(i==0 && userString[i]!=' ')
       {
          firstFound = true;
          firstLetterIndex=i;
       }
       else if(userString[i+1]!=' ' &&
              (userString[i]==' ' || userString[i]=='\0') )
       {
          firstFound = true;
          firstLetterIndex=i+1;
       }

       if ((userString[i]!=' ' && userString[i+1]==' ')|| (userString[i]!=' ' && userString[i+1]=='\0'))
       {
          secondFound = true;
          secondLetterIndex=i;
       }

       if (firstFound == true && secondFound == true)
       {
          ptrArray[counterWords]=&userString[firstLetterIndex];
          userString[secondLetterIndex+1]='\0';
          counterWords++;
          firstFound=false;
          secondFound=false;
       }
   }
}

RandPair generateRandPair()
{
   RandPair pair;

   srand(time(0));
   pair.randNumber1=rand()%counterWords;
   pair.randNumber2=rand()%counterWords;
   return pair;
}

void printWords()
{
   for(int i=0;i<counterWords;i++)
   {
      printf("%s ",ptrArray[i]);
   }
}


int main()
{
   setlinebuf(stdout);

   printf("Enter string\n");
   fgets(userString,ARR_SIZE,stdin);

   getWords();

   for(int i=0;i<MAX_GENERATE;i++)
   {
      RandPair nextPair=generateRandPair();

      char* tmp= ptrArray[nextPair.randNumber1];
      ptrArray[nextPair.randNumber1]=ptrArray[nextPair.randNumber2];
      ptrArray[nextPair.randNumber2]=tmp;
   }

   printWords();

   return 0;
}
