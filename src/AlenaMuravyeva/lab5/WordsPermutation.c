/*
 * WordsPermutation.c
 *
 *  Created on: 21 ���. 2016 �.
 *      Author: Counting
 */

#include "WordsPermutation.h"
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#define MAX_POINTER 40

enum Boolean{false,true};
typedef enum Boolean BOOL;
int len=0;
int counterWords=0;
char *ptrArray [MAX_POINTER];

void getWords(char *userString,char**ptrArray )
{
   int firstLetterIndex=0;
   int secondLetterIndex=0;
   int firstFound=0;
   int secondFound =0;

   int len=strlen(userString);
   userString [len-2]='\0';
   len=len-2;

   for(int i=0;i<len;i++)
   {
       if(i==0 && userString[i]!=' ')
       {
          firstFound = true;
          firstLetterIndex=i;
       }
       else if(userString[i+1]!=' ' &&
              (userString[i]==' ' || userString[i]=='\0') )
       {
          firstFound = true;
          firstLetterIndex=i+1;
       }

       if ((userString[i]!=' ' && userString[i+1]==' ')|| (userString[i]!=' ' && userString[i+1]=='\0'))
       {
          secondFound = true;
          secondLetterIndex=i;
       }

       if (firstFound == true && secondFound == true)
       {
          ptrArray[counterWords]=&userString[firstLetterIndex];
          userString[secondLetterIndex+1]='\0';
          counterWords++;
          firstFound=false;
          secondFound=false;
       }
   }
}

RandPair generateRandPair()
{
   RandPair pair;

   srand(time(0));
   pair.randNumber1=rand()%counterWords;
   pair.randNumber2=rand()%counterWords;
   return pair;
}

void printWords()
{
   for(int i=0;i<counterWords;i++)
   {
      printf("%s ",ptrArray[i]);
   }
}

