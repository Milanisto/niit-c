/*
 ============================================================================
 Name        : lab2.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : The program print the letters first then numbers
 ============================================================================
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define ARR_SIZE 36
#define LETTER_START_ASCII 65
#define LETTER_END_ASCII 90
#define NUMBER_START_ASCII 48
#define NUMBER_END_ASCII 57


int main()
{
  char dictionary[ARR_SIZE]={'0','1','2','3','4','5','6','7','8','9','A','B',
                            'C','D','E','F','G','H','I','J','K','L','M','N',
                            'O','P','Q','R','S','T','V','U','W','X','Y','Z'};
  char randString[ARR_SIZE+1];
  int nextRand;
  int counterFromEnd=ARR_SIZE;
  char tmp=0;

  //Generate rand array
  srand(time(0));
  for (int i=0;i<ARR_SIZE;i++)
  {
    nextRand=rand()%ARR_SIZE;
    randString[i]= dictionary[nextRand];
  }
  randString[ARR_SIZE] = '\0';
  printf("Rand string: %s\n", randString);

  //Grouping: Letters to the left, numbers to the right
  for(int counterFromStart = 0; counterFromStart<counterFromEnd; counterFromStart++)
  {
    if(randString[counterFromStart]>=NUMBER_START_ASCII && randString[counterFromStart]<=NUMBER_END_ASCII)
    {
      while(counterFromEnd>counterFromStart)
      {
        counterFromEnd--;
        if(randString[counterFromEnd]>=LETTER_START_ASCII && randString[counterFromEnd]<=LETTER_END_ASCII)
        {
          tmp=randString[counterFromStart];
          randString[counterFromStart]=randString[counterFromEnd];
          randString[counterFromEnd]=tmp;
          break;
        }
      }
    }
  }

  printf("Result string: %s\n", randString);

  return 0;
}

