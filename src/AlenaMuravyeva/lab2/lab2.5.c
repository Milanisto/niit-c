/*
 ============================================================================
 Name        : lab2.5c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Programs generated password
 ============================================================================
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define ARR_SIZE 62
#define MAX_PASSWORD_ARR_SIZE 9
#define MAX_PASSWORD_SIZE 8
#define GENERATED_PASSWORD_NUMBER 10

int main()
{
   char dictionary[ARR_SIZE]={'0','1','2','3','4','5','6','7','8','9','A','B',
                             'C','D','E','F','G','H','I','J','K','L','M','N',
                             'O','P','Q','R','S','T','V','U','W','X','Y','Z',
                             'a','b','c','d','e','f','g','h','i','j','k','l',
                             'm','n','o','p','q','r','s','t','v','u','w','x',
                             'y','z'};
   char password[MAX_PASSWORD_ARR_SIZE];
   int nextRand;

   srand(time(0));

   for (int j=0;j<GENERATED_PASSWORD_NUMBER;j++)
   {
      for (int i=0;i<MAX_PASSWORD_SIZE;i++)
      {
         nextRand=rand()%ARR_SIZE;
         password[i]= dictionary[nextRand];
      }
      password[MAX_PASSWORD_SIZE] = '\0';
      printf("The generated password: %s\n", password);
   }
   return 0;
}
