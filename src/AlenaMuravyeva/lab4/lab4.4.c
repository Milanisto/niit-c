/*
 ============================================================================
 Name        : lab4.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program to find the longest sequence in the array using pointers
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LETTER 256

int main()
{
   char userString[MAX_LETTER];

   setlinebuf(stdout);

   printf("enter string\n");
   fgets(userString,MAX_LETTER,stdin);

   int userStringLen = strlen(userString);
   //remove /r
   userString[userStringLen-1] = '\0';
   userStringLen--;

   int curLetterCounter = 1;
   int maxLetterCounter = 1;
   char *p;

   for(int i = 0; i<userStringLen; i++)
   {
      if(userString[i] == userString[i+1] )
      {
         curLetterCounter++;
      }
      else
      {
         if(curLetterCounter > maxLetterCounter)
         {
            maxLetterCounter = curLetterCounter;
            p = &userString[i];
         }
         curLetterCounter = 1;
      }
   }

   printf("%d\n",maxLetterCounter);

   for(int i = 0; i<maxLetterCounter; i++)
   {
      printf("%c",*p);
   }

   return 0;
}
