/*
 ============================================================================
 Name        : lab6.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program transforms a number to a string
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_NUMBER 10

char string [MAX_NUMBER]={0};

int conversion (int numberUser)
{
   static int i=0;
   if(numberUser==0)
   {
      return 1;
   }
   else
   {
      int remainderDevision=0;
      remainderDevision=numberUser%10+48;
      numberUser=numberUser/10;
      conversion(numberUser);
      string[i]=remainderDevision;
      i++;
   }
   return 1;
}

int main()
{
   int numberUser=0;

   setlinebuf(stdout);

   printf("Enter the sequence numbers from 1 to 9\n");
   scanf("%d",&numberUser);

   conversion(numberUser);
   printf("%s",string);

   return 0;
}
