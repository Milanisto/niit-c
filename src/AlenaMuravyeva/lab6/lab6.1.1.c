/*
 ============================================================================
 Name        : lab6.1.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Fractal
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING 48
#define MAX_LETTER 48
#define MID MAX_LETTER/2
#define FIGURE_VOL 3
#define DEPTH 2

const char CLEAN_SYM = ' ';

char stars[MAX_STRING][MAX_LETTER];

void printArray ()
{
   int i,j;
   for(i=0;i<MAX_STRING;i++)
   {
      for(j=0; j<MAX_LETTER;j++)
      {
         printf( "%c" ,stars[i][j]);
      }
      printf("\n");
   }
}

void drawSimpleFigure(int x, int y, char star)
{
   if(stars[y][x] == CLEAN_SYM) stars[y][x]=star;
   if(stars[y+1][x] == CLEAN_SYM) stars[y+1][x]=star;
   if(stars[y-1][x] == CLEAN_SYM) stars[y-1][x]=star;
   if(stars[y][x+1] == CLEAN_SYM) stars[y][x+1]=star;
   if(stars[y][x-1] == CLEAN_SYM) stars[y][x-1]=star;
}

int paintStar (int depth,int x,int y, char star)
{
   if(depth<=0)
   {
      return 1;
   }

   drawSimpleFigure(x, y, star);
   drawSimpleFigure(x-FIGURE_VOL, y, star);
   drawSimpleFigure(x+FIGURE_VOL, y, star);
   drawSimpleFigure(x, y-FIGURE_VOL, star);
   drawSimpleFigure(x, y+FIGURE_VOL, star);

   depth--;

   paintStar(depth, x-FIGURE_VOL*3, y, star);
   paintStar(depth, x+FIGURE_VOL*3, y, star);
   paintStar(depth, x, y-FIGURE_VOL*3, star);
   paintStar(depth, x, y+FIGURE_VOL*3, star);

   return 1;
}

int main()
{
   //clean array
   for(int i=0;i<MAX_STRING;i++)
   {
      for(int j=0; j<MAX_LETTER;j++)
      {
         stars[i][j]=CLEAN_SYM;
      }
   }

   char star = '*';
   paintStar(DEPTH, MID, MID, star);
   printArray();
   return 0;
}
