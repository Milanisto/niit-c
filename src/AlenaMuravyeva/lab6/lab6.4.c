/*
 ============================================================================
 Name        : lab6.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : The program compares the computation time of the recursive
               and iterative methods
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <time.h>

clock_t timeStart,timeEnd,runtimeIterative,runtimeRecursively;
int sum=0;

void generateNumber (int *arr,int arrSize)
{
   srand(time(0));
   for(int i=0;i<arrSize;i++)
   {
      arr[i]=rand()%10;
   }
}
clock_t doSumIterative (int *arr,int arrSize)
{
   timeStart= clock();
   for (int i=0; i < arrSize; i++)
   {
      sum += arr[i];
   }
   timeEnd=clock();
   runtimeIterative=timeEnd-timeStart;
   return runtimeIterative;
}
int doSumRecursive(int* arr, int n)
{
   static int res=0;
   if(n<=0)
   {
      return 0;
   }
   else if(n==1)
   {
      return *arr;
   }
   res = doSumRecursive(arr, n/2) + doSumRecursive(arr+n/2, n-n/2);
   return res;
}

int main()
{
   int power=0;
   int base=2;
   int arrSize=0;
   int *arr=0;

   setlinebuf(stdout);

   printf("Enter power\n");
   scanf("%d",&power);
   arrSize=pow(base,power);

   arr=(int*) calloc (arrSize,sizeof(int));
   if(arr==NULL)
   {
      perror("Memory allocation error:");
      return 1;
   }

   generateNumber (arr,arrSize);

   runtimeIterative= doSumIterative (arr,arrSize);

   timeStart=0;
   timeEnd=0;
   timeStart= clock();
   sum=doSumRecursive(arr, arrSize);
   timeEnd=clock();
   runtimeRecursively=timeEnd-timeStart;

   printf ("Sum = %d, iterative time = %f\n",sum,(double)runtimeIterative/CLOCKS_PER_SEC);
   printf("Sum = %d, recursively time=%f\n",sum,(double)runtimeRecursively/CLOCKS_PER_SEC);

   if(runtimeRecursively<runtimeIterative)
      printf ("runtimeRecursively<runtimeIterative\n");
   else if(runtimeRecursively>runtimeIterative)
      printf ("runtimeRecursively>runtimeIterative\n");
   else if(runtimeRecursively==runtimeIterative)
      printf("runtimeRecursively==runtimeIterative\n");

   free(arr);
   return 0;
}
