/*
 ============================================================================
 Name        :lab6.6.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Fibonacci, but without exponentially increasing recursion
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define PREV_1 1
#define PREV_2 2
#define ARR_SIZE 41

unsigned int arr [ARR_SIZE]={0};
int userNumber=0;

void calc (int number)
{
   if(number>userNumber)
   {
      return;
   }

   arr[number]=arr[number-PREV_1]+arr[number-PREV_2];
   calc(++number);
}
void fibonacci ()
{
   arr[0]=0;
   arr[1]=1;

   if(userNumber==0)
   {
      return;
   }

   if(userNumber==1)
   {
      return;
   }

   calc(2);
}

int main()
{
   setlinebuf(stdout);

   printf("Enter number\n");
   scanf("%d",&userNumber);

   fibonacci ();
   printf("Fibonacci from %d = %d\n",userNumber, arr[userNumber]);
   return 0;
}
