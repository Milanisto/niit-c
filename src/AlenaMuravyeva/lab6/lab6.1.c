/*
 ============================================================================
 Name        : lab6.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Fractal
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING 48
#define MAX_LETTER 48
#define MID MAX_LETTER/2
#define FIGURE_VOL 9
#define NUMDER_STARS 5
#define NUMBER 2
#define LATERAL_STARS_2 8
#define REMAINDER_FIGUR 3

char stars[MAX_STRING][MAX_LETTER];

void printArray ()
{
   int i,j;
   for(i=0;i<MAX_STRING;i++)
   {
      for(j=0; j<MAX_LETTER;j++)
      {
         printf( "%c" ,stars[i][j]);
      }
      printf("\n");
   }
}

int paintStar (int n,int x,int y)
{
   int counterStars=0;
   if(n==0)
   {
      return 1;
   }

   //draw horizontal to the right
   for(int j=x,i=y;j<(x+FIGURE_VOL);j++)
   {
      counterStars++;
      stars[i][j]='*';
      if(counterStars==NUMBER ||counterStars==LATERAL_STARS_2)
      {
         stars[i-1][j]='*';
         stars[i+1][j]='*';
      }
   }

      x=x+FIGURE_VOL/NUMBER;
      y=y-FIGURE_VOL/NUMBER;
      counterStars=0;

   //draw vertical line down
   for(int i=y,j=x;i<(y+FIGURE_VOL);i++)
   {
      counterStars++;
      stars[i][j]='*';

      if(counterStars==NUMBER ||counterStars==LATERAL_STARS_2)
      {
         stars[i][j-1]='*';
         stars[i][j+1]='*';
      }
   }

   if(n>REMAINDER_FIGUR)
   {
      y=MID;
      x=x+FIGURE_VOL/NUMBER;
   }
   else if(n==REMAINDER_FIGUR)
   {
      y=MID-FIGURE_VOL;
      x=FIGURE_VOL-1;
   }
   else if(n==NUMBER)
   {
      y=y+FIGURE_VOL*NUMBER+FIGURE_VOL/NUMBER;
      x=FIGURE_VOL-1;
   }
   paintStar(--n,x,y);
   return 1;
}

int main()
{
   for(int i=0;i<MAX_STRING;i++)
   {
      for(int j=0; j<MAX_LETTER;j++)
      {
         stars[i][j]=' ';
      }
   }
   int x=0;
   int y=MID;
   int n=NUMDER_STARS;
   paintStar (n,x,y);
   printArray();
   return 0;
}
