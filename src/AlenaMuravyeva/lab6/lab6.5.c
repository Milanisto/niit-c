/*
 ============================================================================
 Name        : lab6.5.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Fibonacchi
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N_ELEMENT 40
#define PREV_1 2
#define PREV_2 1

int fibonacci (int x)
{
   if(x==0)
   {
      return 0;
   }
   if(x==1)
   {
      return 1;
   }
   else
   {
      return fibonacci(x-PREV_1)+fibonacci(x-PREV_2);
   }
}
int main()
{
   int result=0;
   clock_t timeStart,timeEnd,runtime;
   FILE* fp;
   fp=fopen("fibonacci.csv","wt");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }
   setlinebuf(stdout);
   for(int i = 1; i<=N_ELEMENT; i++)
   {
      timeStart= clock();
      result = fibonacci(i);
      timeEnd=clock();
      runtime=timeEnd-timeStart;
      printf("Fibonacchi[%d] = %d, time=%f\n", i, result,(double)runtime/CLOCKS_PER_SEC);
      fprintf(fp,"%d; %d; %f\n", i, result,(double)runtime/CLOCKS_PER_SEC);
      timeStart=0;
      timeEnd=0;
      runtime=0;
   }
   fclose(fp);
   return 0;
}
