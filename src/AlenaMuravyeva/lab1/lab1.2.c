/*
 ============================================================================
 Name        : lab1.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : The program displays the greeting "Good morning", "Good day","Good evening","Good night".

 ============================================================================
 */

#include <stdio.h>

int main()
{
   int hours=0;
   int minutes=0;
   int seconds=0;

   setlinebuf(stdout);

   printf("Hello.Enter current time 00:00:00\n");
   scanf("%d:%d:%d",&hours,&minutes,&seconds);

   if(hours <24 && minutes <60 && seconds <60)
   {
      if(hours<12 &&hours>=6 && minutes>=0 && seconds>=0)
         printf("Good morning\n");
      else if (hours<17 && hours>=12 && minutes>=0 && seconds>=0)
         printf("Good day\n");
      else if (hours<23 && hours>=17 && minutes>=0 && seconds>=0)
         printf("Good evening\n");
      else if (hours>=23 && minutes>=0 && seconds>=0)
         printf("Good night\n");
      else if (hours<6 && hours>=0 && minutes>=0 && seconds>=0)
         printf("Good night\n");
   }
   else
   {
      printf("Error, please enter correct time\n");
   }
   return 0;
}
