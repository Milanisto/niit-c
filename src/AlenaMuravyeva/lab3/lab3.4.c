/*
 ============================================================================
 Name        : lab3.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program finds the sum of the numbers in the string
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LETTERS 80
#define START_NUMBER_ASKII 48
#define END_NUMBER_ASKII 57
#define MAX_NUMBER_CATEGORY 6

int ConvertCharToInt (char ch)
{
   int retVal=(int)ch;
   if(retVal<START_NUMBER_ASKII ||retVal>END_NUMBER_ASKII)
   {
      retVal=START_NUMBER_ASKII;
   }
   return retVal-START_NUMBER_ASKII;
}
int main()
{
   char stringUserNumber [MAX_LETTERS]={0};
   int sumSequence=0;
   int commonSum=0;
   setlinebuf(stdout);

   printf("Enter numbers\n");
   fgets(stringUserNumber,MAX_LETTERS,stdin);

   int len=strlen(stringUserNumber);
   stringUserNumber[len-2]='\0';
   len=len-2;

   int num1, num2, num3, num4, num5,num6;
   for(int i=0;i<len;i=i+MAX_NUMBER_CATEGORY)
   {
      if(len < i+6)
      {
         commonSum = commonSum + atoi(&stringUserNumber[i]);
         break;
      }
      else
      {
      num1 = ConvertCharToInt(stringUserNumber[i])*100000;
      num2 = ConvertCharToInt(stringUserNumber[i+1])*10000;
      num3 = ConvertCharToInt(stringUserNumber[i+2])*1000;
      num4 = ConvertCharToInt(stringUserNumber[i+3])*100;
      num5 = ConvertCharToInt(stringUserNumber[i+4])*10;
      num6 = ConvertCharToInt(stringUserNumber[i+5]);
      sumSequence = num1+num2+num3+num4+num5+num6;
      commonSum=commonSum+sumSequence;
      }
   }
   printf("The common sum = %d\n",commonSum);
   return 0;
}
