/*
 ============================================================================
 Name        : lab3.7.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program displays the symbols in descending order
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include<string.h>

#define MAX_ASCII 256
#define MAX_LETTERS 80

int main()
{
   unsigned char ASCII [MAX_ASCII];
   int counters [MAX_ASCII];
   char userString[MAX_LETTERS];
   int i=0;
   int hold=0;
   int save;

   for(i=0;i<MAX_ASCII;i++)
   {
      counters [i]=0;
   }

   setlinebuf(stdout);

   printf ("Enter string\n");
   fgets(userString,MAX_LETTERS,stdin);

   for(i=0;i<MAX_ASCII;i++)
   {
      ASCII[i]=i;
   }
   int len=strlen(userString);
   userString[len-2]='\0';
   len=len-2;

   for(i=0;i<len;i++)
   {
      int num=(int)userString[i];
      counters[num]++;
    }

   for(int pass=1;pass<MAX_ASCII;pass++)
   {
      for(int i=0;i<MAX_ASCII-1;i++)
      {
         if(counters[i]<counters[i+1])
         {
            hold=counters[i+1];
            counters[i+1]=counters[i];
            counters[i]=hold;
            save=ASCII[i+1];
            ASCII[i+1]=ASCII[i];
            ASCII[i]=save;
         }
      }
   }

   for(i=0;i<MAX_ASCII;i++)
   {
      if(counters[i]>0 && ASCII[i]!='\0')
      {
         printf("%2c %9d\n",ASCII[i], counters[i]);
      }
   }

   return 0;
}

