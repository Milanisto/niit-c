/*
 ============================================================================
 Name        : lab3.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Programs believes the number words and prints from new line
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define ARR_SIZE 256

int main()
{
   char string [ARR_SIZE];
   int firstLeterIndex=0;
   int secondLeterIndex=0;
   int firstFound = TRUE;
   int secondFound = FALSE;
   int counterWords=0;

   setlinebuf(stdout);

   printf("enter string\n");
   fgets(string,ARR_SIZE,stdin);

   int len=strlen(string);
   //remove \r and \n
   string[len-2] = '\0';
   len=len-2;

    for(int i=0;i<len;i++)
    {
       if(i==0 && string[i]!=' ')
       {
          firstFound = TRUE;
          firstLeterIndex=i;
       }
       else if(string[i+1]!=' ' && string[i]==' ')
       {
          firstFound = TRUE;
          firstLeterIndex=i+1;
       }
       else if ((string[i]!=' ' && string[i+1]==' ')|| (string[i]!=' ' && string[i+1]=='\0'))
       {
          secondFound = TRUE;
          secondLeterIndex=i;
       }
       if (firstFound == TRUE && secondFound == TRUE)
       {
          counterWords++;
          for(int j=firstLeterIndex; j<=secondLeterIndex; j++)
          {
             printf("%c",string[j]);
          }
          printf("\n");

          firstFound=FALSE;
          secondFound=FALSE;
          firstLeterIndex=0;
          secondLeterIndex=0;
       }
   }
   printf("Words: %d\n",counterWords);
   return 0;
}
