/*
 ============================================================================
 Name        : lab3.5.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Programs fills an array with random numbers
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARR_SIZE 256

int main()
{
   int arrN[ARR_SIZE]={0};
   int firstNegativeNumberIndex=0;
   int lastPositiveNumberIndex=0;
   int sum=0;

   setlinebuf(stdout);

   srand(time (0));

   for(int i=0;i<ARR_SIZE;i++)
   {
      arrN[i]=rand()%(ARR_SIZE+1)+(-(ARR_SIZE/2));
   }
   for(int i=0;i<ARR_SIZE;i++)
   {
      if(arrN[i]<0)
      {
         firstNegativeNumberIndex=i;
         break;
      }
   }
   for(int i=ARR_SIZE-1;i>=0;i--)
   {
      if(arrN[i]>=0)
      {
         lastPositiveNumberIndex=i;
         break;
      }
    }
   for(int i=firstNegativeNumberIndex;i<=lastPositiveNumberIndex;i++)
   {
      sum=sum+arrN[i];
   }

   printf("Generated Array: ");
   for(int i=0;i<ARR_SIZE;i++)
   {
      printf("%d ",arrN[i]);
   }

   printf("\nFirst index: %d and number: %d",firstNegativeNumberIndex, arrN[firstNegativeNumberIndex]);
   printf("\nLast index: %d and number: %d",lastPositiveNumberIndex, arrN[lastPositiveNumberIndex]);
   printf("\nSum between %d and %d indexes = %d\n",firstNegativeNumberIndex,lastPositiveNumberIndex,sum);
   return 0;
}
