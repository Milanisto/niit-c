/*
 ============================================================================
 Name        : lab7.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :Sorted symbols
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_ASCII 256

typedef struct TSYM TSYM;

struct TSYM
{
   unsigned char ch;
   float freq;
};

TSYM syms[MAX_ASCII];
int counter=0;

void sorting (TSYM syms[])
{
   float hold=0;
   char save='\0';
   for(int pass=1;pass<MAX_ASCII;pass++)
   {
      for(int i=0;i<MAX_ASCII-1;i++)
      {
         if(syms[i].freq<syms[i+1].freq)
         {
            hold=syms[i+1].freq;
            syms[i+1].freq=syms[i].freq;
            syms[i].freq=hold;
            save=syms[i+1].ch;
            syms[i+1].ch=syms[i].ch;
            syms[i].ch=save;
         }
      }
   }
}
void print (TSYM syms[])
{
   printf("\nSorted symbols: \n");
   printf("----------------\n");
   for(int i=0;i<MAX_ASCII;i++)
   {
      if(syms[i].freq > 0)
         printf("Sorted Symbol: %c -   %f  \n",syms[i].ch,syms[i].freq);
   }
}
void read(FILE *fp,TSYM syms[])
{
   char sym;
   int symNum;
   while(!feof(fp))
  {
     fscanf(fp,"%c",&sym);
     symNum = (int)sym;
     syms[symNum].freq++;
     counter++;
  }

}
int main()
{
   FILE *fp;
   fp=fopen("xyz.txt","rb");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }

   for(int i=0;i<MAX_ASCII;i++)
   {
      syms[i].ch=i;
      syms[i].freq=0;
   }

   read(fp,syms);
   for(int i=0;i<MAX_ASCII;i++)
   {
      syms[i].freq=syms[i].freq/counter;
   }
   sorting (syms);
   print (syms);

   fclose(fp);
   return 0;
}
