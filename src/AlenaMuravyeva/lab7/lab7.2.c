/*
 ============================================================================
 Name        : lab7.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program analyzes the file C and displays the table of
              occurrence of keywords of the language
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 50

enum Boolean{false,true};
typedef enum Boolean BOOL;

FILE *fp;
FILE *fpf;

struct NODE
{
   int count;
   char word[SIZE];
   struct NODE *left;
   struct NODE *right;
};
struct NODE * makeTree(struct NODE *root,char *word)
{
   if(root==NULL)
   {
      struct NODE *temp=(struct NODE*)malloc(sizeof(struct NODE));
      temp->count=0;
      strcpy(temp->word,word);
      temp->left=temp->right=NULL;
      return temp;
   }
   else if(strcmp(root->word,word)>0)
   {
      root->left=makeTree(root->left,word);
      return root;
   }
   else if(strcmp(root->word,word)<0)
   {
      root->right=makeTree(root->right,word);
      return root;
   }
   else
   {
      root->count++;
      return root;
   }
}
BOOL openDefinedFiles(char** argv)
{
   BOOL result=true;

   fp=fopen(argv[1],"rt");
   if(fp==NULL)
   {
      perror("File:");
      result = false;
   }

   fpf=fopen(argv[2],"rt");
   if(fpf==NULL)
   {
      perror("File:");
      result = false;
   }

   return result;
}
BOOL searchTree(struct NODE *root, char* words)
{
   if(root == NULL)
   {
      return false;
   }
   if(strcmp(root->word,words)>0)
   {
      return searchTree(root->left,words);
   }
   else if(strcmp(root->word,words)<0)
   {
      return searchTree(root->right,words);
   }
   else if(strcmp(root->word,words)==0)
   {
      root->count++;
      return true;
   }
   return false;
}
void printTree(struct NODE *root)
{
   if(root)
   {
      if(root->left)
         printTree(root->left);
      printf("%s - %d\n",root->word,root->count);
      if(root->right)
         printTree(root->right);
   }
}
int main(int argc, char *argv[])
{
   char buf[SIZE];
   char words[SIZE];
   struct NODE *root=NULL;

   openDefinedFiles(argv);

   while(!feof(fp))
   {
      fscanf(fp,"%s",buf);
      root=makeTree(root,buf);
   }

   while(!feof(fpf))
   {
      fscanf(fpf,"%s",words);
      searchTree(root,words);
   }

   printTree(root);

   fclose(fp);
   fclose(fpf);

   return 0;
}
