/*
 ============================================================================
 Name        : lab7.1.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "regins.h"

int main()
{
   FILE* fp;
   int count=0;
   char buf[512];
   PITEM head,tail,item;

   fp=fopen("fips10_4.csv","rt");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }
   setlinebuf(stdout);

   while(fgets(buf,512,fp))
   {
      if(count==0)
      {
         head=createList(createName(buf));
         tail=head;
      }
      else
      {
         tail=addToTail(tail,createName(buf));
      }
      count++;
   }
   fclose(fp);
   printf("Total items: %d\n", countList(head));
   item=findByAbr(head,"AD");
   printName(item);
   item=findByName(head,"Dubayy");
   printName(item);
   return 0;
}
