/*Write a program that for selected line displays the longest
his word length*/

#include <stdio.h>
#include <string.h>

#define N 80

void GetLane(char *lane);
void FindMax(char *lane, int *charnum);
void PrintWord(int p, char *lane, int flag);

int main() 
{
	int charnum = 0;
    char userlane[N];
	GetLane(userlane);
	FindMax(userlane, &charnum);
    printf(" The longest word. There are %d characters.\n", charnum);
	return 0;
}
void GetLane(char *lane)
{
	printf("Enter the string.\n");
	fgets(lane, N, stdin);
	lane[strlen(lane) - 1] = 0;
}
void FindMax(char *lane, int *charnum)
{
	int i = 0;
	int count = 0;
	int p = 0;
	for (i = 0; lane[i]; i++)
	{
		if (lane[i] != ' ') count++;
		else
		{
			if (count > (*charnum))
			{
				(*charnum) = count;
				p = i - count;
			}
			count = 0;
		}
	}
		PrintWord(p,lane,(*charnum));
}
void PrintWord(int p, char *lane, int flag)
{
	if(flag)
	{
		putchar(lane[p]);
		p++;
		flag--;
		PrintWord(p,lane, flag);
	}
}
