/*Write a program that prompts the user for a string consisting
multi-word and an integer n, and then removes the n - th word from a string.
 an error message in case of incorrect n*/

#include <stdio.h>
#include <string.h>

#define SIZE 80

void GetLane(char *lane);
void GetNumWord(int *num, char *lane);
void CountingWords(char *lane, int *count);
void DeletWord(int num, char *lane);
void MoveLane(int num, char *lane);

int main()
{
	char userlane[SIZE];
	int n = 0;
	GetLane(userlane);
	GetNumWord(&n, userlane);
	DeletWord(n, userlane);
}
void GetLane(char *lane)
{
	printf("Enter your lane.\n");
	fgets(lane, SIZE, stdin);
	lane[strlen(lane) - 1] = 0;
}
void GetNumWord(int *num, char *lane)
{
	int count = 0;
	printf("Enter number of word.\n");
	scanf("%d", num);
	CountingWords(lane, &count);
	if ((*num) > count || (*num) < 1)
		printf("Error! Uncorrect number!");
}
void CountingWords(char *lane, int *count)
{
	int i = 0;
	while (1)
	{
		if (lane[i])
		{
			if (lane[i] != ' ' && lane[i + 1] == ' ' || lane[i] != ' ' && lane[i + 1] == 0)
			{
				(*count)++;
			}
			i++;
		}
		else
			break;
	}
}
void DeletWord(int num, char *lane)
{
	int i = 0;
	int count = 0;
	while (1)
	{
		if (count == (num - 1))
		{
			while (1)
			{
				MoveLane(i, lane);
				if (lane[i] == ' ' || lane[i] == '\0')
					break;
			}
			printf("%s", lane);
			break;
		}
		if (lane[i])
		{
			if (lane[i] != ' ' && lane[i + 1] == ' ' || lane[i] != ' ' && lane[i + 1] == 0)
			{
				count++;
			}
			i++;
		}
	   
	}
}
void MoveLane(int num, char *lane)
{
	lane[num] = lane[num + 1];
	num++;
	if (lane[num] != '\0')
		MoveLane(num, lane);
}