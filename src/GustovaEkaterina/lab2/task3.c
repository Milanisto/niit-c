//Write a program that displays on the screen of a triangle of stars

#include<stdio.h>
void RequestNumLines(int *numlines);
void InitialSpases(int *spase, int numline);
void InputTriangle( int *stars, int *spase);
void PutChars(int flag, char sym);

int main()
{
	int numlines;
	int stars = 1;
	int spase;
	RequestNumLines(&numlines);
	InitialSpases(&spase,numlines);
	InputTriangle(&stars,&spase);
	return 0;
}
void RequestNumLines(int *numlines)
{
	printf("Enter the number of rows.\n");
	scanf("%d", numlines);
}
void InitialSpases(int *spase, int numline)
{
	(*spase) = numline - 1;
}
void InputTriangle( int *stars, int *spase)
{
	const char *SymForBuild[] = { ' ','*','\n' };
	int flagSp;
	int flagSt;
	if ((*spase)>= 0)
	{
		flagSp = (*spase);
		flagSt = (*stars);
		PutChars(flagSp, SymForBuild[0]);
		PutChars(flagSt, SymForBuild[1]);
		putchar(SymForBuild[2]);
		(*stars) += 2;
		(*spase)--;
		InputTriangle(stars, spase);
	}
}
void PutChars(int flag, char sym) 
{
	
	while (flag > 0)
	{
		putchar(sym);
		flag--;
	}
}

	    
	
