//Write a program to reposition the symbols in the array according to the rule :
//first there are letters, then numbers.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

#define SIZE 20

void FillingLane(char *str);
char comp(const char *, const char *);
void indexfornumandchar(int*indexnum, int*indexchar, char *lane);
char compfornumandchar(const char *, const char *);


int main()
{
	char userlane[SIZE];
	int indexnum;
	int indexchar;
    srand (time(NULL));
	FillingLane(userlane);
	puts(userlane);
	qsort(userlane, SIZE, sizeof(char), (char(*) (const void *, const void *)) comp);
	indexfornumandchar(&indexnum, &indexchar, userlane);
	qsort(userlane, indexchar, sizeof(char), (char(*) (const void *, const void *)) compfornumandchar);
	qsort(userlane, indexnum, sizeof(char), (char(*) (const void *, const void *)) compfornumandchar);
	puts(userlane);
	return 0;

}
void FillingLane(char *str)
{
	int i = 0;
	char buf;
	for (i = 0; i < SIZE;)
	{
		buf = rand() % 122;
		if (isalnum(buf))
		{
			str[i] = buf;
			i++;
		}
	}
	str[SIZE - 1] = 0;
}
char comp(const char *i, const char *j)
{
	return (-1*(*i - *j));
}
void indexfornumandchar(int*indexnum, int*indexchar, char *lane)
{
	(*indexchar) = 0;
	int i = 0;
	for (i = 0; i < SIZE; i++)
		if (isalpha(lane[i]))
			(*indexchar)++;
	(*indexnum) = SIZE - (*indexchar);
}
char compfornumandchar(const char *i, const char *j)
{
	return (*i - *j);
}

	