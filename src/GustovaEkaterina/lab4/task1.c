/*Write a program that allows the user to enter several
strings to the keyboard, and then outputting them in ascending order
of the length of the string.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZESTRING 80
#define QUANTILYSTRINGS 10

void InputStrings(char (*string)[SIZESTRING]);
int comp(const void *, const void *);

int main()
{
	char userstrings[QUANTILYSTRINGS][SIZESTRING];
	InputStrings(userstrings);
	return 0;
}
void InputStrings(char (*string)[SIZESTRING])
{
	char *pointer[QUANTILYSTRINGS];
	int count = 0;
	int i = 0;
	while (count < QUANTILYSTRINGS && *fgets((string[count]), SIZESTRING, stdin) != '\n')
	{
		string[count][strlen(string[count]) - 1] = '\0';
		pointer[count] = string[count];
		count++;
	}
	qsort(pointer, count, sizeof(pointer[0]), comp);
	for(i = 0; i < count; i++)
	{
		printf("%s\n", pointer[i]);
	}
}
int comp(const void *i, const void *j)
{
	return strlen(*(char**)i) - strlen(*(char**)j);
}
