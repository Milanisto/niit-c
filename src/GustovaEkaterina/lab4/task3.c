/*Write program, which asks for a string, and determines not
Whether string palindrome (reads the same from left to right and from right to left)*/

#include <stdio.h>
#include <string.h>

#define SIZE 80

void GetLane(char*);
void FindDifference(char*);

int main()
{
	char userline[SIZE];
	GetLane(userline);
	FindDifference(userline);
}
void GetLane(char*line)
{
	puts("Enter your line:");
	fgets(line, SIZE, stdin);
	line[strlen(line) - 1] = 0;
}
void FindDifference(char*line)
{
	char *startp = &line[0];
	char *endp = &line[strlen(line) - 1];
	while (1)
	{
		 if (startp == &line[(strlen(line) - 1) / 2])
		{
			puts("Palindrome");
			break;
		}
		 else if (*startp == *endp)
		{
			startp++;
			endp--;
		}
		else if(*startp != *endp)
		{
			puts("Not Palindrome");
			break;
		}
	}
}
