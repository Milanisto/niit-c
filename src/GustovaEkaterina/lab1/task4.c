// translation feet and inches to centimeters
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	float ft, inches;
	const float inchesInFt = 12;
	const float coefForInches = 2.54;
	float centimeters;
	char datarequest[] = "Enter size.(Feet and Inches by a space)";
	do
	{
		puts(datarequest);
		scanf("%f %f", &ft, &inches);
		do 
		{
			getchar();
			getchar();
		} while (ft == EOF || inches == EOF);
	} while (ft < 0 || inches < 0);	
	centimeters = (ft * inchesInFt + inches)*coefForInches;
	printf("%.1f cm\n", centimeters);
	return 0;
}
