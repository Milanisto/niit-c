/* Практикум 4. Задача 6.
Написать программу, которая запрашивает количество родственников в семье,
а потом позволяет ввести имя родственника и его возраст. Программа должна
определить самого молодого и самого старого родственника
Замечание:
Нужно завести массив строк для хранения имён и два указателя: young и old,
которые по мере ввода, связывать с нужными строками.
*/

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#define N 10
#define M 80

int main()
{
	char name[N][M];
	char *young,*old;
	int i=0,n=0,age=0,old_age=0,young_age=200;;

	printf("Enter the number of family members (maximum 10 members): ");
	scanf("%d",&n);

	for(i;i<n;i++)
	{
		printf("Enter the name of the relative and his age (for example, Ivan 10):\n");
		scanf("%s%d",&name[i],&age);
		
		if(age<young_age)
		{
			young=name[i];
			young_age=age;
		}

		if(age>old_age)
		{
			old=name[i];
			old_age=age;
		}
	}

	printf("\nThe youngest name: %s, his age - %d years.",young,young_age);
	printf("\nThe oldest name: %s, his age - %d years.\n",old,old_age);

	return 0;
}