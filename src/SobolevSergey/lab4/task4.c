/* Практикум 4. Задача 4.
Изменить программы для поиска самых длинных последовательностей в мас-
сиве с использованием указателей вместо числовых переменных.
Замечание:
Аналогично можно изменить тексты всех программ с использованием массивов
так, чтобы доступ к данным осуществлялся через указатели.

*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{
	char string[N];
	char *startWord;
	int i,count=0,max=0;

	puts("Enter a string:");
	fgets(string,N,stdin);

	for(i=0;string[i];i++)
		if(string[i]!=' '&&string[i]==string[i+1])
			count++;
		else
		{
			if(count > max)
			{
				max=count;
				startWord=&string[i-count];
			}
			count=0;
		}

	printf("%d - ",max+1);

	for(i=startWord; i<startWord+max+1; i++)
		putchar(*startWord);

	printf("\n");

	return 0;
}