/* Практикум 6. Задача 5.
Написать программу, которая измеряет время вычисления N-ого
члена ряда Фибоначчи. Предусмотреть вывод таблицы значений
для N в диапазоне от 1 до 40 (или в другом диапазоне по желанию)
на экран и в файл
Замечание:
Текстовый файл со значениями можно открыть в электронной таблице и по-
строить график зависимости времени от члена ряда N
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 40

long long fib(int n)
{
	if(n==1 || n==2)
		return 1;
	else
		return fib(n-1)+fib(n-2);
}

int main( )
{
	clock_t start,stop;
	FILE * fp;
	int i;
	long long f;
	
	fp=fopen("output.txt","wt");
	if(fp==NULL)
	{
		perror("File:");
		return 1;
	}
	
	for(i=1; i<=N; i++)
	{
		start=clock( );
		f=fib(i);
		stop=clock( );
		printf("%2d %10lld %3f\n",i,f,(double)(stop-start)/CLOCKS_PER_SEC);
		fprintf(fp,"%d; %lld; %f\n",i,f,(double)(stop-start)/CLOCKS_PER_SEC);
	}

	fclose(fp);
	return 0;
}