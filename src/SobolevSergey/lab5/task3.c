/* Практикум 5. Задача 3.
Написать программу, переставляющую случайным образом симво-
лы каждого слова каждой строки текстового файла, кроме первого
и последнего, то есть начало и конец слова меняться не должны.
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки выполняется разбивка на слова и независимая обра-
ботка каждого слова.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define M 256

int bufRandom(char* buf,int lenWord)
{
	int i,left=0,right=0,temp=0;
	
	if(lenWord>3)
	{
		for(i=0;i<lenWord;i++)
		{
			do
			{
				left=rand( )%(lenWord-2)+1;
				right=rand( )%(lenWord-2)+1;
			} while(left==right);

			temp=buf[left];
			buf[left]=buf[right];
			buf[right]=temp;
		}
	}
	return 0;
}

void printWord(char *string,int lenString)
{
	int i=0,j,lenWord=0;
	char buf[M]={0};

	while(i<lenString)
	{
		lenWord=0;
		j=0;
		while(string[i]!=' '&&string[i]!='\0')
		{
			buf[j]=string[i];
			lenWord++;
			i++;
			j++;
		}

		bufRandom(buf,lenWord);

		for(j=0;j<lenWord;j++)
			printf("%c",buf[j]);
		
		putchar(' ');
		i++;
	}
	printf("\n");
}

int main( )
{
	int lenString=0;
	char string[M];

	srand(time(NULL));
	
	FILE* fp;

	fp=fopen("input.txt","rt");
	if(fp==NULL)
	{
		perror("File:");
		return 1;
	}
	srand(time(NULL));

	while(fgets(string,sizeof(string),fp))
	{
		lenString=strlen(string)-1;
		string[strlen(string)-1]=0;

		printWord(string,lenString);
	}

	printf("\n");
	fclose(fp);
	return 0;
}