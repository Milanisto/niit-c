/* Практикум 5. Задача 1.
Написать программу, которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.
Замечание:
Программа должна состоять минимум из трех функций:
a) printWord - выводит слово из строки (до конца строки или пробела)
b) getWords - заполняет массив указателей адресами первых букв слов
c) main - основная функция
*/

#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>

#define M 256

int getWords(char* string,char** startWord,int len);
int randomIndex(int* index,int countWord);
void printWord(char *string,char** startWord,int* index,int countWord);

int main( )
{
	int len=0;
	char string[M];
	char *startWord[M]={0};
	int index[M/2]={0};
	int countWord;

	puts("Enter a string:");
	fgets(string,M,stdin);
	
	len=strlen(string)-1;
	string[strlen(string)-1]=0;
	
	countWord=getWords(string,startWord,len);
	randomIndex(index,countWord);
	printWord(string,startWord,index,countWord);

	return 0;
}