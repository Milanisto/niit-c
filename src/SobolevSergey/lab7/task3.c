/* Практикум 7. Задача 3.
Написать программу, которая строит таблицу встречаемости сим-
волов для произвольного файла, имя которого задаётся в команд-
ной строке. Программа должна выводить на экран таблицу встре-
чаемости, отсортированную по убыванию частоты

Замечание:
В программе необходимо определить структурный тип SYM, в котором нужно
хранить код символа и частоту встречаемости (вещественное число от 0 до 1).
После анализа файла, массив структур SYM должен быть отсортирован по
частоте.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct sym 
{
	int symbol;
	float count;
};

int main(int argc,char *argv[])
{
	int i=0, j=0;
	int character;
	int k=0;
	int count_k=0;
	float count[256]={0};

	struct sym arr[256];
	for(i=0;i<256;i++)
	{
		arr[i].symbol=0;
		arr[i].count=0;
	}

	FILE *fp=fopen(argv[1],"rt");
	while((character=fgetc(fp))!=EOF)
	{
		count_k++;
		for(i=0; i<256; i++)
		{
			if(character==arr[i].symbol)
			{
				count[i]++;
				break;
			}
			if(arr[i].symbol==0)
			{
				arr[i].symbol=character;
				count[i]=1;
				k++;
				break;
			}
		}
	}
	fclose(fp);

	for(i=0; i<k; i++)
		arr[i].count=(float)count[i]/count_k;

	for(i=1; i<k; i++)
		for(j=0; j<k-1; j++)
			if(arr[j].count<arr[j+1].count)
			{
				float temp=arr[j].count;
				arr[j].count=arr[j+1].count;
				arr[j+1].count=temp;

				temp=arr[j].symbol;
				arr[j].symbol=arr[j+1].symbol;
				arr[j+1].symbol=temp;
			}

	for(i=0; i<k; i++)
		printf("Symbol: %c - repeated: %f\n",arr[i].symbol,arr[i].count);
	
	return 0;
}