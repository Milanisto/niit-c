/* Практикум №3. Задача 1. 
Написать программу, подсчитывающую количество слов во введён-
ной пользователем строке
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{	
	char string[N];
	int i, count=0;

	puts("Enter a string:");
	fgets(string,N,stdin);
	string[strlen(string)-1]=0;
	
	for(i=0;i<strlen(string)-1;i++)
	{
		if((string[i]==' ')&&(string[i+1]!=' '))
			count++;
	}

	if(string[0]!=' ')
		count++;

	printf("The number of words in the string - %d\n",count);
	
	return 0;
}
