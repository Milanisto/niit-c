/* Практикум 3. Задача 5.
Написать программу, которая формирует целочисленный массив размера N, а
затем находит сумму элементов, расположенным между первым отрицатель-
ным и последним положительным элементами.
Замечание:
Массив заполняется случайными числами: отрицательными и положительны-
ми поровну (или почти поровну...)
*/

#define _CRT_SECURE_NO_WARNINGS
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 10

int main( )
{
	int string[N];
	int i,j,first_negative=0,last_positive=N-1,sum=0;;
	srand(time(NULL));

	for(i=0; i<N; i++)
		string[i]=(rand( )%2) ? -rand( )%9-1 : rand( )%9+1;

	for(i=0; i<N; i++)
		printf("%d ",string[i]);
	printf("\n");

	for(i=0; i < N; i++)
		if(string[i]<0)
		{
			first_negative=i;
				break;
		}
	printf("The first negative element: %d to i=%d\n",string[first_negative],first_negative);

	for(j=N-1; j > 0; j--)
		if(string[j] > 0)
		{
			last_positive=j;
			break;
		}
	printf("The last positive element: %d to i=%d\n",string[last_positive],last_positive);

	for(i=first_negative+1; i<=last_positive-1; i++)
		sum+=string[i];

	printf("Sum of elements: %d\n",sum);

	return 0;
}
