/* Практикум 3. Задача 10.
Написать программу, которая запрашивает у пользователя строку, состоящую
из нескольких слов и целое число n, а затем удаляет n - ое слово из строки. В
случае неккоректного n выводится сообщение об ошибке
Замечание:
В результате работы программы символы должны быть удалены из массива.
Создавать дополнительные массивы нельзя
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 256

int main( )
{
	char string[N];
	int i,n=0,startWord=0,len=0,numberWord=0;

	puts("Enter a string:");
	fgets(string,N,stdin);
	len=strlen(string);

	printf("Enter number of word: ");
	scanf("%d",&n);

	for(i=0;i<len;i++)
	{
		if((string[i]!=' ' && string[i+1]==' ')||(string[i]!=' ' && string[i+1]=='\0'))
		{
			numberWord++;
		}

		if((n!=1)&&(string[i]!=' '&&string[i-1]==' '))
			startWord=i;
		if(n==1&&string[0]!=' ')
			startWord=0;

		if(numberWord==n)
		{
			while(string[i]!='\0')
			{
				string[startWord]=string[i+1];
				startWord++;
				i++;
			}
			printf("\n%s",string);
			break;
		}
	}

	if(n>numberWord)
		printf("Error! Repeat the input string and the search number word.");

	puts("\n\n");
	return 0;
}
