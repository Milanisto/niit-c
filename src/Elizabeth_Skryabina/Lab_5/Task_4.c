/*print random words (file)
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 256

int GetWords(char *str, char **pwords);
void PrintWord(char **pwords, int count);

int main()
{
	char str[N] = { 0 };
	char *pwords[N];
	int count = 0, numRandWord;
	srand(time(NULL));
	FILE *fp;
	fp = fopen("Task_4_read.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	while (fgets(str, N, fp) != 0)
	{
		str[strlen(str) - 1] = 0;
		count = GetWords(str, pwords);
		while (count > 0)
		{
			numRandWord = rand() % count;
			PrintWord(pwords, numRandWord);
			pwords[numRandWord] = pwords[count - 1];
			count--;
		}
		putchar('\n');
	}
	fclose(fp);
	return 0;
}

int GetWords(char *str, char **pwords)
{
	int count = 0, inWord = 0, i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pwords[count++] = str + i;
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	return count;
}

void PrintWord(char **pwords, int count)
{
	char *p;
	{
		p = pwords[count];
		while ((*p != ' ') && (*p != '\0'))
			putchar(*p++);
		putchar(' ');
	}
}