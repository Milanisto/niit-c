/*Sum between first negative and last positive numbers
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 10

void createarray(int *numb);

int main()
{
	int numb[MAX] = { 0 };
	int posnum = 0, negnum = MAX - 1, sum = 0;
	srand(time(NULL));
	createarray(numb);
	while (numb[posnum] >= 0)
		posnum++;
	printf("\nFirst negative: %d\n", numb[posnum]);
	posnum++;
	while (numb[negnum]<0)
		negnum--;
	printf("Last positive: %d\n", numb[negnum]);
	negnum--;
	for (posnum; posnum <= negnum; posnum++)
		sum += numb[posnum];
	printf("\nsumm = %d\n", sum);
	return 0;
}

void createarray(int *numb)
{
	int i;
	for (i = 0; i<MAX; i++)
	{
		numb[i] = rand() % 40 - 20;
		printf("%d  ", numb[i]);
	}
}