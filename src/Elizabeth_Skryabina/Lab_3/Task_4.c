#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define maxlen 5

int main()
{
	char str[256] = { 0 };
	char *p = str;
	int len = 0, number = 0, sum = 0;
	printf("Enter your text with num:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
	while (*p != '\0')
	{
		while (*p != '\0' && *p<'0' || *p>'9')
			p++;
		while (*p >= '0' && *p <= '9' && len < maxlen)
		{
			number = number * 10 + ((int)*p-'0');
			p++;
			len++;
		}
		sum += number;
		number = 0;
		len = 0;
	}
	printf("sum = %d", sum);
	return 0;
}