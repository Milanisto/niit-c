/*search and print words
*/
#include <stdio.h>
#include <string.h>
#define N 256

void EnterString(char *str);
int GetWords(char *str, char **pwords);
void PrintWord(char **pwords, int count);

int main()
{
	char str[N] = { 0 };
	char *pwords[N];
	int count = 0;
	EnterString(str);
	count = GetWords(str, pwords);
	PrintWord(pwords, count);
	return 0;
}

void EnterString(char *str)
{
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;
}

int GetWords(char *str, char **pwords)
{
	int count = 0, inWord = 0, i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pwords[count++] = str + i;
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	return count;
}

void PrintWord(char **pwords, int count)
{
	char *p;
	int i;
	printf("in the text %d words:\n", count);
	for (i = 0; i < count; i++)
	{
		p = pwords[i];
		printf("%d - ", i + 1);
		while ((*p != ' ') && (*p != '\0'))
			putchar(*p++);
		putchar('\n');
	}
}