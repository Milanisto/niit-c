#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void PrintWord(char *pmaxWord);

int main()
{
	char str[256] = { 0 };
	char *pmaxWord = str;
	int maxWord = 0, max = 1, i = 0;
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = 0;

	while (str[i])
	{
		if (str[i] == str[i+1])
		{
			max++;
			i++;
		}
		else {
			if (max > maxWord)
			{
				i++;
				maxWord = max;
				pmaxWord = &str[i - maxWord];
				max = 1;
			}
			else {
				max = 1;
				i++;
			}
		}
	}
	PrintWord(pmaxWord, maxWord);
	return 0;
}
void PrintWord(char *pmaxWord, int maxWord)
{
	printf("%d ", maxWord); 
	for (maxWord; maxWord > 0; maxWord--)
		putchar(*pmaxWord++);
	putchar('\n');
}