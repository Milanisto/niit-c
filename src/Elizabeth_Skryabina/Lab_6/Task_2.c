/* 6.2 Collatz sequence
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

unsigned long int Collatz(unsigned long int i);

int main()
{
	unsigned long int n, max_sequence = 0, max_numb = 0, value;
	for (n = 2; n <= 1000000; n++)
	{
		value = Collatz(n);
		if (max_sequence < value)
		{
			max_sequence = value;
			max_numb = n;
		}
	}
	printf("in the range of 2-1000000\nlongest sequence Collatz = %lu, with n = %lu \n", max_sequence, max_numb);
	return 0;
}

unsigned long int Collatz(unsigned long int i)
{
	if (i != 1)
	{
		if (i % 2 != 0)
			return (1 + Collatz(3 * i + 1));
		else
			return (1 + Collatz(i / 2));
	}
	else
		return 1;
}