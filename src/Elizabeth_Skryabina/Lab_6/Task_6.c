/* 6.5 Fibonacci2
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

typedef unsigned long long ULL;
void FibonacciSumm(ULL *Summ, ULL LastSumm, int i, int n);
ULL Fibonacci(int n);

int main()
{
	int n;
	printf("Enter the number to calculate Fibonacci: (not more than 92): ");
	scanf("%d", &n);
	printf("Fibonacci = %lld\n", Fibonacci(n));
	return 0;
}

void FibonacciSumm(ULL *Summ, ULL LastSumm, int i, int n)
{
	ULL Result = *Summ;
	if (i <= n)
	{
		*Summ += LastSumm;
		FibonacciSumm(Summ, Result, i + 1, n);
	}
}

ULL Fibonacci(int n)
{
	ULL Summ = 1;
	if (n == 0)
		return 0;
	else if (n <= 2)
		return 1;
	else
	{
		FibonacciSumm(&Summ, 0, 2, n);
		return Summ;
	}
}