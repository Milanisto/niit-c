/* delete excess spaces
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int delbeginspace(char *str);
int delmiddlespace(char *str);
int delendspace(char *str);

int main()
{
	char str[256] = { 0 };
	printf("(delete excess spaces)\n");
	printf("Enter your text:\n");
	fgets(str, 256, stdin);
	str[strlen(str) - 1] = '\0';
	delbeginspace(str);
	delmiddlespace(str);
	delendspace(str);
	printf("%s\n", str);
	return 0;
}

int delbeginspace(char *str)
{
	int i = 0, j, lengthstr;
	lengthstr = strlen(str);
	while (str[i] == ' ')
	{
		for (j = 1; j<lengthstr; j++)
			str[j - 1] = str[j];
		str[lengthstr] = '\0';
	}
	return 0;
}

int delmiddlespace(char *str)
{
	int i = 0, j, lengthstr;
	lengthstr = strlen(str);
	while (str[i] != '\0')
	{
		while ((str[i] != ' ') && (str[i] != '\0'))
			i++;
		if (str[i + 1] == ' ')
			for (j = i + 1; j<lengthstr; j++)
				str[j] = str[j + 1];
		else
			i++;
	}
	return 0;
}

int delendspace(char *str)
{
	int lengthstr;
	lengthstr = strlen(str);
	if (str[lengthstr - 1] == ' ')
		str[lengthstr - 1] = '\0';
	return 0;
}