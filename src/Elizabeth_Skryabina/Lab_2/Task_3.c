/* triangle of *
*/
#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>

int main()
{
	int numline, i, numsimv, numspace;
	printf("Enter number of lines: ");
	scanf("%d", &numline);
	for (i = 1; i <= numline; i++)
	{
		for (numspace = numline - i; numspace>0; numspace--)
			putchar(' ');
		for (numsimv = 0; numsimv <= i + i - 2; numsimv++)
			putchar('*');
		putchar('\n');
	}
	return 0;
}