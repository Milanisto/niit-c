#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
	int f, d, c;
	float sm;
	while (1)
	{
		printf("Enter your height in inches (format - x'y, where x-feet, y-inches) ");
		scanf("%d'%d", &f, &d);
		if ((f < 0) || (d < 0) || (d >= 12))
		{
			printf("Not correct! Try again!\n");
			do {
				c = getchar();
			} while (c != '\n' && c != EOF);
		}
		else
			break;
	}
	d += f * 12;
	sm = d*2.54;
	printf("%d'%d = %4.1f sm\n", f, d -= f * 12, sm);
	return 0;
}