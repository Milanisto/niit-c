#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct SYM
{
	unsigned char ch;
	float fleq;
};

swap(struct SYM *x, struct SYM *y)
{
	struct SYM temp;
	temp = *x; 
	*x = *y; 
	*y = temp; 
}

int main(int argc, char *argv[])
{
	struct SYM Symbols[256] = { 0 };
	unsigned char ReadSymbol;
	int i,j, count = 0, diff_count=0;

	FILE *fp = fopen(argv[1], "rb"); // read file and filling structure
	while (!feof(fp))
	{
		ReadSymbol = getc(fp);
		count++;
		i = 0;
		for (i = 0; i <= count; i++)
		{
			if (ReadSymbol == Symbols[i].ch)
			{
				Symbols[i].fleq++;
				break;
			}
			if (Symbols[i].ch == '\0')
			{
				Symbols[i].ch = ReadSymbol;
				Symbols[i].fleq++;
				diff_count++;
				break;
			}
		}
	}
	fclose(fp);

	for (i = 0; i <= diff_count; i++) // frequency
		Symbols[i].fleq = Symbols[i].fleq / count;

	for (i = 0; i<diff_count - 1; i++) // sort
		for (j = diff_count - 1; j>i; j--)
			if (Symbols[j - 1].fleq <Symbols[j].fleq )
				swap(&Symbols[j - 1], &Symbols[j]);
	
	i = 0;
	while (Symbols[i].ch != '\0') // print
	{
		printf("%c - %f\n", Symbols[i].ch, Symbols[i].fleq);
		i++;
	}
}