#define _CRT_SECURE_NO_WARNINGS
#include "Task_2_Keywords.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	struct NODE *root = NULL; //������� ������ ������
	FILE *fp = fopen("keywords.txt", "rt");

	char buf[SIZE]; //��������� ����� ��� ����
	while (!feof(fp))
	{
		fscanf(fp, "%s", buf);
		root = makeTree(root, buf);
	}
	fclose(fp);
	fp = fopen(argv[1], "rt");
	while (!feof(fp))
	{
		fscanf(fp, "%s", buf);
		searchTree(root, buf);
	}
	fclose(fp);
	int max;
	max = search_max(root);
	while (max >= 0)
	{
		printTree_sort(root,max);
		max--;
	}
	return 0;
}