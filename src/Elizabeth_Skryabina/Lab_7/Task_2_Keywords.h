#pragma once
#define SIZE 256

struct NODE
{
	int count;
	char word[SIZE]; //������ ��� �������� �����
	struct NODE *left; //��������� ������ ����� � ������ �����
	struct NODE *right;
};

struct NODE * makeTree(struct NODE *root, char *word);
void printTree(struct NODE *root);
struct NODE searchTree(struct NODE *root, char * buf);
int search_max(struct NODE *root);
int printTree_sort(struct NODE *root, int maxcount);
