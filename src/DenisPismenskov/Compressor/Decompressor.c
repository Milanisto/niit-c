
#include "Compressor.h"

void Decompressor(char fp_in[])
{
	FILE *fpin, *fp101, *fpout;

	SYM simbols[256] = { 0 };
	SYM *psyms[256];

	int i = 0, count = 0, tail;
	
	char fp_101[256] = { 0 }, fp_out[256] = { 0 };
				
	fpin = OpenFile(fp_in, "rb");
	fp101 = OpenFile(FileExtension(EditableFileExtension(fp_in, fp_101), fp_101, ".101"), "wb");
	
	tail = ReadHeader(fpin, &count, simbols);
	
	ReadFile(fp101, fpin, tail);
	fclose(fp101);
	fclose(fpin);
	
	i = 0;
	while (simbols[i].ch != '\0') 
	{
		psyms[i] = &simbols[i];
		i++;
	}
	struct SYM *root = buildTree(psyms, count); 
	makeCodes(root); 
		
	fp101 = OpenFile(fp_101, "rb");
	fpout = OpenFile(EditableFileExtension(fp_101, fp_out), "wb");
	while (!feof(fp101))
		fputc(Unpacking(fp101, root), fpout);
	
}

char *EditableFileExtension(const char original[], char editable[])
{
	int k = strlen(original), i = 0;
	while (k >= i)
	{
		while (original[k] != '.')
			k--;
		break;
	}
	for (i = 0; i < k; i++)
		editable[i] = original[i];
	return editable;
}

int ReadHeader(FILE *fpin, int *count, struct SYM *psym)
{
	char header[8];
	char chtail, ch;
	int i, tail;
	fread(&header, sizeof(char), 7, fpin);
	fread(&ch, sizeof(char), 1, fpin);
	*count = (int)ch;
	for (i = 0; i < *count; i++) 
	{
		fread(&psym[i].ch, sizeof(char), 1, fpin);
		fread(&psym[i].freq, sizeof(float), 1, fpin);
	}
	fread(&chtail, sizeof(char), 1, fpin);
	tail = (int)chtail;
	return tail;
}

void ReadFile(fp101, fpin, tail)
{
	char buf[8], 
		prev_RealSymbol;
	int ReadSymbol, i;
	
	prev_RealSymbol = fgetc(fpin);
	while ((ReadSymbol = fgetc(fpin)) != EOF)
	{
		code.ch = prev_RealSymbol;
		fputc(code.byte.b1 + '0', fp101);
		fputc(code.byte.b2 + '0', fp101);
		fputc(code.byte.b3 + '0', fp101);
		fputc(code.byte.b4 + '0', fp101);
		fputc(code.byte.b5 + '0', fp101);
		fputc(code.byte.b6 + '0', fp101);
		fputc(code.byte.b7 + '0', fp101);
		fputc(code.byte.b8 + '0', fp101);
		prev_RealSymbol = ReadSymbol;
	}
	code.ch = prev_RealSymbol;
	buf[0] = code.byte.b1 + '0';
	buf[1] = code.byte.b2 + '0';
	buf[2] = code.byte.b3 + '0';
	buf[3] = code.byte.b4 + '0';
	buf[4] = code.byte.b5 + '0';
	buf[5] = code.byte.b6 + '0';
	buf[6] = code.byte.b7 + '0';
	buf[7] = code.byte.b8 + '0';
	for (i = 0; i < tail; i++)
		fputc(buf[i], fp101);
}

unsigned char Unpacking(FILE *fp101, struct SYM *root)
{
	int ch;
	ch = fgetc(fp101);
	if (ch == '0' && root->left != NULL && root->right != NULL)
		return Unpacking(fp101, root->left);
	if (ch == '1' && root->left != NULL && root->right != NULL)
		return Unpacking(fp101, root->right);
	if (root->left == NULL && root->right == NULL)
		ungetc(ch, fp101);
	return root->ch;
}
