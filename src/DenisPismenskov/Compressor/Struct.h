#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct SYM 
{
	unsigned char ch; 
	float freq; 
	char code[256]; 
	struct SYM *left; 
	struct SYM *right; 
};

typedef struct SYM SYM;

union CODE 
{
	unsigned char ch;
	struct 
	{
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
		
	} byte;
};

union CODE code;

FILE* OpenFile(char name_file[], const char mode[]);
char* FileExtension(const char source[], char dest[], const char extension[]);
int Table(FILE *fp_in, struct SYM *psyms);
struct SYM *buildTree(struct SYM *psym[], int N);
void makeCodes(struct SYM *root);
void Coder(FILE *fp_in, FILE *fp_101, struct SYM *psym, int diff_count);
int Tail(FILE *fp_101, int *sizeControlFile);
void Header(FILE *fp_archive, int diff_count, struct SYM *psym, int tail);
unsigned char pack(unsigned char buf[]);
void Packing(FILE *fp_101, FILE * fp_archive, int sizeControlFile, int tail);


char *EditableFileExtension(const char source[], char dest[]);
int ReadHeader(FILE *fp_archive, int *diff_count, struct SYM *psym);
void ReadFile(fp_101, fp_archive, tail);
char Unpacking(FILE *fp_101, struct SYM *root);

void Compressor(char FileName_fp_in[]);
void Decompressor(char FileName_fp_archive[]);