#include "Compressor.h"


FILE* OpenFile(char file[], const char mode[])
{
	FILE* fp = fopen(file, mode);
	if (fp == NULL)
	{
		perror("Error: ");
		return 0;
	}
	return fp;
}

char* FileExtension(const char source[], char dest[], const char extension[])
{
	strcpy(dest, source);
	strcat(dest, extension);
	return dest;
}

struct SYM * buildTree(struct SYM *psym[], int N)
{
	struct SYM *temp = (struct SYM*)malloc(sizeof(struct SYM)); 
	temp->freq = psym[N - 2]->freq + psym[N - 1]->freq; 
	temp->left = psym[N - 1]; 
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	if (N == 2) 
		return temp;
	else 
	{
		for (int i = 0; i < N; i++)
			if (temp->freq > psym[i]->freq)
			{
				for (int j = N - 1; j > i; j--)
					psym[j] = psym[j - 1];
				psym[i] = temp;
				break;
			}
	}
	return buildTree(psym, N - 1);
}

void makeCodes(struct SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}
