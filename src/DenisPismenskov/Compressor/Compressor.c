
#include "Compressor.h"

void Compressor(char fp_in[])
{
	
	FILE *fpin, *fp101, *fpout;

	SYM simbols[256] = { 0 }; 
	SYM *psyms[256]; 

	int i = 0, count = 0, sizesimbols = 0, tail; 
	

	char fp_101[256] = { 0 }, fp_out[256] = { 0 };
	
	fpin = OpenFile(fp_in, "rb");
	fp101 = OpenFile(FileExtension(fp_in, fp_101, ".101"), "wb");
	
	count = Table(fpin, simbols); //������ ��������� ����� � �������� ������� �������������
	
	
	for (int i = 0; i<count; i++) // ��������� ���������� �� ���������
	{
		psyms[i] = &simbols[i];
	}
	
	SYM *root = buildTree(psyms, count); //����� ������� �������� ������ �������
	makeCodes(root); 
		
	rewind(fpin); //���������� ��������� �� ������ �����
	Coder(fpin, fp101, simbols, count); //� ����� ������ �������� ����, � ���������� ���������� ���� � ������������� ����
	fclose(fpin);
	fclose(fp101);
	
	fp101 = OpenFile(fp_101, "rb"); //��������� ��� ������
	tail = Tail(fp101, &sizesimbols);
	
	fpout = OpenFile(FileExtension(fp_in, fp_out, ".asd"), "wb");
	Header(fpout, count, simbols, tail); //��������� ��������� ����� 
	
	rewind(fp101); //���������� ��������� �� ������ �����
	Packing(fp101, fpout, sizesimbols, tail);
	fclose(fp101);
	fclose(fpout);
	printf("\nArchiving completed!\n");
}




int Table(FILE *fpin, struct SYM *simbols) 
{
	int i, j,
		ch, // ������� ������
		quantity = 0, // ������� �������� 
		count = 0;
	int kolvo[256] = { 0 };
	SYM temp;
	while ((ch = fgetc(fpin)) != EOF) 
	{
		quantity++;
		for (i = 0; i <= quantity; i++)
		{
			if (ch == simbols[i].ch)
			{
				kolvo[i]++;
				break;
			}
			if (simbols[i].ch == '\0')
			{
				simbols[i].ch = ch;
				kolvo[i]++;
				count++;
				break;
			}
		}
	}
	for (i = 0; i < count; i++) // ������� ������� 
		simbols[i].freq = (float)kolvo[i] / quantity;
	for (i = 0; i < count - 1; i++) // ���������� �� ������� 
		for (j = count - 1; j > i; j--)
			if (simbols[j - 1].freq < simbols[j].freq)
			{
				temp = simbols[j - 1];
				simbols[j - 1] = simbols[j];
				simbols[j] = temp;
			}
	return count;
}


void Coder(FILE *fpin, FILE *fp101, struct SYM *psym, int count)
{
	int i,
		ch;
	while ((ch = fgetc(fpin)) != -1) 
	{
		for (i = 0; i < count; i++)
			if (ch == psym[i].ch)
			{
				fputs(psym[i].code, fp101); 
				break; // ��������� �����
			}
	}
}

int Tail(FILE *fp101, int *sizesimbols)
{
	int	ch, tail;
	while ((ch = fgetc(fp101)) != EOF)
		(*sizesimbols)++;
	tail = *sizesimbols % 8; //������� �������
	return tail;
}

void Header(FILE *fpout, int count, struct SYM *psym, int tail)
{
	int i;
	fwrite("archive", sizeof(char), 7, fpout); //�������� �������
	fwrite(&count, sizeof(char), 1, fpout); //���������� ���������� ��������
	for (i = 0; i < count; i++) //���������� ������� �������������
	{
		fwrite(&psym[i].ch, sizeof(char), 1, fpout);
		fwrite(&psym[i].freq, sizeof(float), 1, fpout);
	}
	fwrite(&tail, sizeof(char), 1, fpout); //�������� ������
}

unsigned char pack(unsigned char buf[])
{
	
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

void Packing(FILE *fp101, FILE * fpout, int sizesimbols, int tail)
{
	char buf[8];
	int i, j = 0;
	for (i = 0; i < (sizesimbols - tail); i++) //�������� �������� �����
	{
		buf[j] = getc(fp101);
		if(j == 7)
		{
			fputc(pack(buf), fpout);
			j = 0;
		}
		else j++;
	}
	if (tail != 0) //�������� �����
	{
		for (i = 0; i < 8; i++)
			buf[i] = 0;
		j = 0;

		while (j < tail)
		{
			buf[j] = getc(fp101);
			j++;
		}
		fputc(pack(buf), fpout);
	}
}
