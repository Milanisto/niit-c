/*��������� ��������� ������ ,����������� �� ���������� �����,�� �����������.
��������� ������ ��������� ����� ������������ � ����.*/

#include "stdio.h"
#include "string.h"

#define N 64
#define M 10

int main()
{
	char * p[N],* temp;
	FILE * fpr,* fpw;
	int i=0,count=0,j,min;
	char str[M][N];

	fpr=fopen("Task5_r.txt","rt");                     //��������� ���� �� ������
	if(fpr==NULL) {
		perror("File:");
		return 1;
	}

	while ((p[count]=fgets(str[count],N,fpr))!=NULL )  //��������� ������ ����������
		count++;
		
	for(i=0;i<count-1;i++){                           //��������� ������
		min=i;
		for(j=i; j<count;j++){
			if(strlen(p[j]) < strlen(p[min])){
				temp=p[min];p[min]=p[j];p[j]=temp;
			}
		}
	}
	
	
	fpw=fopen("Task5_w.txt","r+");                     //��������� ���� �� ������
	if(fpw==NULL) {
		perror("File:");
		return 1;
	}
	
	for(i=0;i<count;i++)                              //���������� ������ � ����         
		fputs(p[i],fpw);
		
	printf("The program is completed successfully\n");
	fclose(fpr);
	fclose(fpw);
	return 0;
}