/*��������� ����������� ��� ����������� � ��� �������,����� ���������� ������ �������� �
������ ������� �����������.��������� ������ ����� ��� �������� ���� � ��� ���������� young � old,
�������,�� ���� �����,����������� � ������� ��������.*/


#include "stdio.h"
#include "string.h"
#include "locale.h"

#define N 64
#define M 10

int main()
{
	char * young, * old;
	int age_young=150,age_old=0,num=0,age;
	char str[M][N];
	int ch;
	
	setlocale(LC_ALL,"rus");
	
	while (1){
		printf("������� ��� �����������\n");
		scanf("%s",str[num]);
		printf("������� �������\n");
		scanf("%d", &age);
		if(age < age_young){
			age_young = age;
			young = str[num];
		}
		if(age > age_old){
			age_old = age;
			old = str[num];
		}
		num++;
		do{
		printf("�� ����� ���������� �����������, 'y' -��, 'n' - ���\n");
		ch = getchar();
		}
		while((ch != 'y') && (ch != 'n'));
		
		if (ch == 'y')
			break;
		
	}

	printf("����� ������� � ����� %s, ��� ������� %d\n",old,age_old);
	printf("����� ������� � ����� %s, ��� ������� %d\n",young,age_young);
	
	return 0;
}