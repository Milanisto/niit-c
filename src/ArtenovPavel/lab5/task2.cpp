/*
��������� "�����������".������� �� ����� �����������,������������ �� ����������� �������������
��������� '*'.����������� ������������ � ��������� ���������� �������, � ����� ��� ����� � �����������
���������� � ��������� ��� �����.
*/
#include "stdio.h"
#include "time.h"
#include "conio.h"
#include "stdlib.h"
#include "windows.h"

#define star '*'
#define space ' '
#define M 41
#define N 60

void clearArr(char (*arr)[N]);
void fillArr(char (*arr)[N]);
void printArr(char (*arr)[N]);

int main()
{
	char arr[M][N];
	srand(time(NULL));
	while(1){
	clearArr(arr);
	fillArr(arr);
	system("cls");
	printArr(arr);
	Sleep(1500);
	}
	
}

void clearArr(char (*arr)[N])
{
	int i,j;
	for (i=0;i<M;i++)
		for(j=0;j<N;j++)
			arr[i][j]=space;
	
}

void fillArr(char (*arr)[N])
{
	int countStar=10;
	int i=0,j=0;
	while (countStar>0){                                     
		i=rand() % 20;
		j=rand() % 30;
		if(arr[i][j]=space){
			arr[i][j]=star;
			countStar--;
		}
	}
	
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			if(arr[i][j]==star){
				arr[i][N-1-j]=star;                         
				arr[M-1-i][j]=star;                         
				arr[M-1-i][N-1-j]=star;                          
			}
		}
	}
}

void printArr(char (*arr)[N])
{
	int i,j;
	for(i=0;i<M;i++){
		for(j=0;j<N;j++)
			printf("%c",arr[i][j]);
		printf("\n");
	}
	
}







