/*
Написать программу, которая запрашивает у пользователя пол,
рост и вес, а затем анализирует соотношение роста и веса, выда-
вая рекомендации к дальнейшим действиям (похудеть, потолстеть
*/


#include <stdio.h>

int main()
{
    float	weight = 0, 
                lenght = 0;
	
    char        bufclear = 0, 
                gender = 0,	
                restart = 0;
	
    while(1)
    {
        printf("Input your weight in kg: ");

        if((scanf("%f", &weight) != 1) || (weight <= 0))
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after float
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        printf("Input your lenght in cm: ");

        if((scanf("%f", &lenght) != 1) || (lenght <= 0) )
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after float
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
               scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        printf("Input your gender (m - male / f - female): ");

        if((scanf("%c", &gender) != 1) || ((gender != 'm') && (gender != 'f')))
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after char
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        if(gender == 'm')
            if(weight > lenght - 100.)
                printf("You NEED LOSE weight\n");
            else if(weight == lenght - 100.)
                printf("Your weight is NORMAL\n");
            else
                printf("You NEED ADD weight\n");
        else
            if(weight > lenght - 110.)
                printf("You NEED LOSE weight\n");
            else if(weight == lenght - 110.)
                printf("Your weight is NORMAL\n");
            else
                printf("You NEED ADD weight\n");

        while(1)
        {
            printf("Restart? (y/n):");

            if((scanf("%c", &restart) != 1) || ((restart != 'y') && (restart != 'n')))
            {
                printf("Wrong input\n");
                if(restart != '\n')
                    while(bufclear != '\n')
                        scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

            //check symbols after char
            scanf("%c", &bufclear);
            if(bufclear != '\n')
            {
                printf("Wrong input\n");
                while(bufclear != '\n')
                    scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;

            if(restart == 'y')
                break;
            else
                return 0;
        }
    }
}