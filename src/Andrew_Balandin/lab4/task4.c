/*
Изменить программы для поиска самых длинных последовательностей в мас-
сиве с использованием указателей вместо числовых переменных.
Замечание:
Аналогично можно изменить тексты всех программ с использованием массивов
так, чтобы доступ к данным осуществлялся через указатели
*/

#include <stdio.h>

#define N 256

int main()
{
    int     i = 0,
            nsymb = 0,
            nmaxsymb = 0;
    
    char*   pprevch,
        *   pmaxch;
    
    char    str[N];
    
    printf("Input your string: ");
    fgets(str, N, stdin);
    
    pprevch = str;
    for(i = 0; *(str + i) != '\n'; i++)
    {
        if(str[i] == *pprevch)
            nsymb++;
        if(str[i] != *pprevch)
        {
            pprevch = &str[i];
            nsymb = 1;
        }
        if(nsymb > nmaxsymb)
        {
            nmaxsymb = nsymb;
            pmaxch = &str[i];
        }
    }
    
    printf("%d ", nmaxsymb);

    for(i = 0; *pmaxch == *(pmaxch + i); i--)
        putchar(*(pmaxch + i));
    putchar('\n');
    
    return 0;
}