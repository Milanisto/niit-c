/*
nаписать программу ”Калейдоскоп”, выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек ’*’.
Изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
Замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение
’*’)
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка
7) Переход к шагу 1
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define RED 1
#define GREEN 2
#define BLUE 6
#define N 20
#define M 79
#define NSTARS 39//5
#define SPACE ' '
#define STAR '*'
#define TIMES_OF_REPEATS 17
#define DELAY 3
#define COMMAND "clear"  // "clear" for *nix, "cls" for win

void clearArr(char arr[][M], int n)
{
    int i,
        j;
    
    for(i = 0; i < n; i++)
        for(j = 0; j < M; j++)
            arr[i][j] = SPACE;
}

void figToArr1(char arr[][M], int kn, int km)
{
    arr[kn]     [km]        = RED;
    arr[kn + 1] [km]        = RED;
    arr[kn + 1] [km + 1]    = RED;
    arr[kn]     [km + 1]    = RED;
}

void figToArr2(char arr[][M], int kn, int km)
{
    arr[kn]     [km]        = GREEN;
    arr[kn + 1] [km]        = GREEN;
    arr[kn + 1] [km + 1]    = GREEN;
    //arr[kn] [km + 1]        = GREEN;
}

void figToArr3(char arr[][M], int kn, int km)
{
    arr[kn]     [km]        = BLUE;
    //arr[kn + 1] [km]        = BLUE;
    arr[kn + 1] [km + 1]    = BLUE;
    arr[kn]     [km + 1]    = BLUE;
}

int isFree(const char arr[][M], int n, int m, int kn, int km)
{
    int i,
        j;
    
    for(i = kn; i <= kn + 1; i++)
        for(j = km; j <= km + 1; j++)
            if(arr[i][j] != SPACE)
                return FALSE;
    
    if(kn >= n - 1 || km >= m - 1)
        return FALSE;
    
    return TRUE;
}

void fillLeftUpPartArrWithFig(char arr[][M], int n, int nstars)
{
    int i,
    j,
    k;
    
    void (*pf[3])(char arr[][M], int, int);
    
    pf[0] = figToArr1;
    pf[1] = figToArr2;
    pf[2] = figToArr3;
    
    clearArr(arr, n);
    
    k = 0;
    while(k < nstars)
    {
        i = rand() % (N / 2);
        j = rand() % (M / 2);
        
        if(isFree(arr, N / 2, M / 2, i, j))
        {
            pf[rand() % 3](arr, i, j);
            k++;
        }
    }
}

void fillLeftUpPartArrAloneStars(char arr[][M], int n, int nstars)
{
    int i,
        j,
        k;
    
    clearArr(arr, n);
    
    k = 0;
    while(k < nstars)
    {
        i = rand() % (N / 2);
        j = rand() % (M / 2);
        
        if(arr[i][j] == SPACE)
        {
            arr[i][j] = STAR;
            k++;
        }
    }
}

void copyToOtherPartsArr(char arr[][M], int n)
{
    int i,
        j;
    
    for(i = 0; i < n / 2; i++)
        for(j = 0; j < M / 2; j++)
            if(arr[i][j] != SPACE)
            {
                arr[N - 1 - i]  [j]         = arr[i][j];
                arr[N - 1 - i]  [M - 1 - j] = arr[i][j];
                arr[i]          [M - 1 - j] = arr[i][j];
            }
}

void printArr(const char arr[][M], int n)
{
    int i, j;
    
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < M; j++)
            if(arr[i][j] == SPACE)
                putchar(arr[i][j]);
            else
                printf("\x1b[3%d;1m%c" "\x1b[0m", arr[i][j], STAR);
        putchar('\n');
    }
}

void sleep(int tsec) //sec
{
    int time = clock() + tsec * CLOCKS_PER_SEC;
    while(clock() <= time)
        ;
}

int main()
{
    int i;
    char arr[N][M];
    
    srand(time(NULL));
    
    for(i = 0; i < TIMES_OF_REPEATS; i++)
    {
        fillLeftUpPartArrWithFig(arr, N, NSTARS);
        //fillLeftUpPartArrAloneStars(arr, N, NSTARS);
        copyToOtherPartsArr(arr, N);
        printArr(arr, N);
        sleep(DELAY);
        system(COMMAND);
    }
    
    return 0;
}