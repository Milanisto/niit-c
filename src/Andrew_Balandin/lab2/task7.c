/*
Написать программу, выводящую таблицу встречаемости символов для введен-
ной пользователем строки. В этой таблице содержится символ строки и число
его повторений.
Замечание:
В этой программе мы стремимся к экономии времени, так что использование
дополнительных массивов оправдано.
*/

#include <stdio.h>
#define N_SIMB 255
#define MAX_STR 256

int main()
{
    
    int alph[N_SIMB][2] = {{0},{0}};
    int i, j;
    char input[MAX_STR] = {0};
    
    for(i = 0; i < N_SIMB; i++)
        alph[i][0] = i + 1;

    fgets(input, MAX_STR, stdin);
    for(i = 0; input[i] != '\n'; i++)
        for(j = 0; j < N_SIMB; j++)
            if(input[i] == alph[j][0])
            {
                alph[j][1]++;
                break;
            }
    for(i = 0; i < N_SIMB; i++)
    {
        if(alph[i][1])
            printf("'%c'\t%d \n", alph[i][0], alph[i][1]);
    }
    return 0;
}