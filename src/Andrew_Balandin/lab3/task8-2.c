/*
Написать программу, которая запрашивает у пользователя строку, состоящую
из нескольких слов и целое число n, а затем выводит n - ое слово строки на
экран. В случае неккоректного n выводится сообщение об ошибке
 */

#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    int    i = 0,
                    j = 0,
                    n = 0,
                    nword = 0,
                    inword = FALSE;
    
    char            str[N] = {0};
    
    printf("Input your text: ");
    fgets(str, N, stdin);
    
    printf("Input number of word: ");
    scanf("%d", &n);
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            nword++;
        }
        if(isspace(str[i]) && inword)
        {
            inword = FALSE;
            if(nword == n)
            {
                j = i - 1;
                while(!isspace(str[j]) && j >= 0)
                    j--;
                for(j++; !isspace(str[j]) && j >= 0; j++)
                    putchar(str[j]);
                putchar('\n');
                return 0;
            }
        }
    }

    printf("Not number for word!!!\n");
    return 0;
}