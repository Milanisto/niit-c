/*
Написать программу, которая печатает таблицу встречаемости символов для
введённой строки, отсортированную по убыванию частоты
Замечание:
Таблица выводится таким образом, что сначала идут самые многочисленные
символы, а затем по мере убывания.
*/

#include <stdio.h>

#define N 256

int main()
{
    int i = 0,
        j = 0,
        tmp;
    char ch;
    int symb[N][2];
    //char str[N];
    
    for(i = 0; i < N; i++)
    {
        symb[i][0] = i;
        symb[i][1] = 0;
    }
    printf("Input your line: ");
    //for(i = 0;(str[i] = getchar()) != EOF && str[i] != '\n'; i++)
    while((ch = getchar()) != EOF && ch != '\n')
    {
        for(j = 0; j < N; j++)
            if(ch == j)
            {
                symb[j][1]++;
                break;
            }
    }
    
    //sort
    for(i = 0; i < N; i++)
    {
        for(j = i + 1; j < N; j++)
            if(symb[j][1] > symb[i][1])
            {
                tmp = symb[i][0];
                symb[i][0] = symb[j][0];
                symb[j][0] = tmp;
                
                tmp = symb[i][1];
                symb[i][1] = symb[j][1];
                symb[j][1] = tmp;
            }
    }
    
    for(i = 0; i < N; i++)
        if(symb[i][1])
            printf("'%c' - %d\n", symb[i][0], symb[i][1]);
}