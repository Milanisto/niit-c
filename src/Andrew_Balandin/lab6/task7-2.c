/*
 *генерирует лабиринт N x M (нечетные!!!) - записывает его в файл lab.txt
 *вход в левом верхнем углу, выход - нижний правый. Вход можно установить 
 *символом '*' где угодно, выход - выход за границы - тоже где угодно.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WALL '#'
#define MAN '*'
#define FREE ' '
#define PATH '.'
#define BLOCK '@'
#define TRUE 1
#define FALSE 0
#define N 21
#define M 73

int k[] = {-2, 2, 0, 0};//how we go in gnawLab
int l[] = {0, 0, -2, 2};
int p[] = {-1, 1, 0, 0};//how we gnaw
int o[] = {0, 0, -1, 1};


void clearLab(char **lab, int arrN, int arrM)
{
    for(int i = 0; i < arrN; i++)
        for(int j = 0; j < arrM; j++)
            lab[i][j] = FREE;
}

void cageLab(char **lab, int arrN, int arrM)
{
    clearLab(lab, arrN, arrM);
    for(int i = 0; i < arrN; i++)
        for(int j = 0; j < arrM; j += 2)
            lab[i][j] = WALL;
    for(int i = 0; i < arrN; i += 2)
        for(int j = 0; j < arrM; j++)
            lab[i][j] = WALL;
}

void printLab(char **lab, int arrN, int arrM, FILE *fp)
{
    for(int i = 0; i < arrN; i++)
    {
        for(int j = 0; j < arrM; j++)
            putc(lab[i][j], fp);
        putc('\n', fp);
    }
    printf("\n\n\n");
}

int isFreeCellNear(char **lab, int n, int m, int arrN, int arrM)
{
    for(int i = 0; i < 4; i++)
        if(n + k[i] < arrN && m + l[i] < arrM && n + k[i] > 0 && m + l[i] > 0 && lab[n + k[i]][m + l[i]] == FREE)
            return TRUE;
    return FALSE;
}

int searchFreeCellNear(char **lab, int *n, int *m, int arrN, int arrM)
{
    for(*n = 1; *n < arrN; *n += 2)
        for(*m = 1; *m < arrM; *m += 2)
            if(isFreeCellNear(lab, *n, *m, arrN, arrM))
                return TRUE;
    return FALSE;
}

void gnawLab(char **lab, int n, int m, int arrN, int arrM)
{
    //printLab(lab, N, M);
    lab[n][m] = BLOCK;
    if(isFreeCellNear(lab, n, m, arrN, arrM))
    {
        int i = rand() % 4;
        if(n + k[i] < arrN && m + l[i] < arrM && n + k[i] > 0 && m + l[i] > 0 && lab[n + k[i]][m + l[i]] == FREE)
        {
            lab[n + p[i]][m + o[i]] = BLOCK;
            gnawLab(lab, n + k[i], m + l[i], arrN, arrM);
        }
        else
        {
            //return;
            gnawLab(lab, n, m, arrN, arrM);
        }
    }
    else if(searchFreeCellNear(lab, &n, &m, arrN, arrM))
    {
        gnawLab(lab, n, m, arrN, arrM);
    }
    else
        return;
}

void removeBlocks(char **lab, int arrN, int arrM)
{
    for(int i = 0; i < arrN; i++)
        for(int j = 0; j < arrM; j++)
            if(lab[i][j] == BLOCK)
                lab[i][j] = FREE;
}

void buildLab(char **lab, int arrN, int arrM)
{
    cageLab(lab, N, M);
    
    gnawLab(lab, 1, 1, N, M);
    
    removeBlocks(lab, N, M);
    
    lab[1][1] = MAN;
    lab[arrN - 1][arrM - 2] = FREE;
}

int main()
{
    char **lab;
    FILE *fp;
    fp = fopen("lab.txt", "wt");
    if(!fp)
        return -1;
    
    lab = (char**) malloc(N * sizeof(char*));
    for(int i = 0; i < N; i++)
        lab[i] = (char*) malloc(M * sizeof(char));
    
    srand(time(NULL));
    
    buildLab(lab, N, M);
    printLab(lab, N, M, stdout);
    printLab(lab, N, M, fp);
    
    fclose(fp);

    for(int i = 0; i < N; i++)
        free(lab[i]);
    free(lab);    
    
    return 0;
}