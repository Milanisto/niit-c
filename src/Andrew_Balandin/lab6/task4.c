/*
Написать программу, которая суммирует массив традиционным и
рекурсивным способами
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int sumIter(int * arr, int begin, int end)
{
    int sum = 0;
    while(begin <= end)
        sum += arr[begin++];
    return sum;
}

int pow2(int n)
{
    return n == 0 ? 1 : pow2(n - 1) * 2;
}

int sumRecur(int * arr, int begin, int end)
{
    if(end == begin)
        return arr[begin];
    else
        return sumRecur(arr, begin, (end + begin) / 2) 
                + sumRecur(arr, (end + begin) / 2 + 1, end);
}

void fillArr(int * arr, int n)
{
    int i;
    for(i = 0; i < n; i++)
        arr[i] = rand() % 10;
}

void printArr(int * arr, int n)
{
    int i;
    for(i = 0; i < n; i++)
        printf("%d ", arr[i]);
    putchar('\n');
}

clock_t clock_of_sum(int * arr, int n, int(*pf)(int*, int, int))
{
    clock_t tbegin, tend;
    tbegin = clock();
    pf(arr, 0, n - 1);
    tend = clock();
    return tend - tbegin;
}

int main()
{
    int * parr = NULL;
    long n,
        sumiter,
        sumrecur,
        titer,
        trecur;
    printf("Input exp of 2, number of arr's elements (the best ~30): ");
    scanf("%d", &n);
    n = pow2(n);
    parr = malloc(n * sizeof(int));
    srand(time(NULL));
    fillArr(parr, n);
    //printArr(parr, n);
    titer = clock();
    sumrecur = sumRecur(parr, 0, n - 1);
    titer = clock() - titer;
    trecur = clock();
    sumiter = sumIter(parr, 0, n - 1);
    trecur = clock() - trecur;

    printf ("Sum iter: %ld \n", sumiter);
    printf ("Sum recur: %ld \n", sumrecur);
    printf ("T iter: %ld \n", titer);
    printf ("T recur: %ld \n", trecur);
    free(parr);
    return 0;
}