/*
Написать программу, которая вычисляет целочисленный результат
арифметического выражения, заданного в виде параметра команд-
ной строки. Предусмотреть поддержку 4-х основных операций. По-
рядок вычисления определяется круглыми скобками
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define N 100

char * firstNotDigit(const char * str)//return first-non digit in str
{
    if(isdigit(*str))
        return firstNotDigit(str + 1);
    else
        return str;
}

char * findRoot(const char * str)//if str has no brackets return NULL
{
    char * sign = NULL;
    int count = 0;
    int i;
    
    for(i = 0; str != '\0'; i++)
    {
        if(count == 1 && sign != NULL && *(sign + 1) != '(')//if str has many sets of brackets
            return str;
        if(count == 1 && sign == NULL && isdigit(*(str)) )//if str has one set of brackets
            //return firstSign(str + 1);
            return firstNotDigit(str);
        if(*str == '\0')//if str has no set of brackets
            return NULL;
        if(*str == '(')
            count++;
        else if(*str == ')')
            count--;
        else if(*str == '+' || *str == '-' || *str == '*' || *str == '/')
            sign = str;
        str++;
    }
}

char partition(const char * buf, char exprl[], char exprr[])//if buf has only digits then records it to exprl and return digit
{
    int i;
    char * proot = findRoot(buf);
    char sign;
    
    if(proot == NULL)
    {
        for(i = 0; buf[i] != '\0'; i++)//copy digits to exprl
            exprl[i] = buf[i];
        exprl[i] = '\0';
        return buf[0];
    }
    
    sign = *proot;
    for(i = 0; (buf + i + 1) != proot; i++)//copy left part
        exprl[i] = buf[i + 1];
    exprl[i] = '\0';
    
    for(i = 1; *(proot + i) != '\0'; i++)//copy right part
        exprr[i - 1] = proot[i];
    exprr[i - 2] = '\0';
    
    return sign;
}

int eval(char * buf)
{
    char exprl[N];
    char exprr[N];
    char sign = partition(buf, exprl, exprr);
    
    if(isdigit(sign))
        return atoi(exprl);
    else
    {
        switch(sign)
        {
            case '+':
                return eval(exprl) + eval(exprr);
            case '-':
                return eval(exprl) - eval(exprr);
            case '*':
                return eval(exprl) * eval(exprr);
            case '/':
                return eval(exprl) / eval(exprr);
            default:
                break;
        }
    }
}

int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        printf("No arguments!\n");
        return 1;
    }
    else
    {
        printf("%d\n", eval(argv[1]));
        return 0;
    }
}