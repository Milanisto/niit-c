/*
Написать программу, которая находит выход из лабиринта
лабиринт "читается" из файла lab.txt - может быть любого размера
*/

#include <stdio.h>
#include <stdlib.h>

#define WALL '#'
#define MAN '*'
#define FREE ' '
#define PATH '.'
#define BLOCK '@'
#define TRUE 1
#define FALSE 0

int k[] = {-1, 1, 0, 0};//how we go in findPath
int l[] = {0, 0, -1, 1};

void sizeOfLab(FILE * fp, int * arrN, int * arrM)
{
    *arrN = *arrM = 0;
    int ch;
    while(fgetc(fp) != '\n')//length of lab
            (*arrM)++;
    (*arrN)++;
    while((ch = fgetc(fp)) != EOF)//height 
            if(ch == '\n')
                (*arrN)++;
    rewind(fp);
}

void readLab(char ** arr, FILE * fp, int arrN, int arrM)
{
    for(int i = 0; i < arrN; i++)
    {
        for(int j = 0; j < arrM; j++)
            arr[i][j] = fgetc(fp);
        fgetc(fp);
    }
}

void printDebug(const char ** arr, int arrN, int arrM)
{
    int i, j;
    for(i = 0; i < arrN; i++)
    {
        for(j = 0; j < arrM; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
    putchar('\n');
}

void printLab(const char ** arr, int arrN, int arrM)
{
    int i, j;
    for(i = 0; i < arrN; i++)
    {
        for(j = 0; j < arrM; j++)
            if(arr[i][j] == BLOCK)
                putchar(FREE);
            else
                putchar(arr[i][j]);
        putchar('\n');
    }
    putchar('\n');
}

void findMan(const char ** arr, int * n, int * m) // find coordinates of MAN '*'
{
    if(arr[*n][*m] == MAN)
        return;
    else if(arr[*n][*m] == '\0')
    {
        (*n)++;
        *m = 0;
    }
    else
        (*m)++;
    findMan(arr, n, m);
}

int findPath(char ** arr, int n, int m, int arrN, int arrM)
{
    if(n >= arrN - 1 || m >= arrM - 1 || n <= 0 || m <= 0)
    {
        arr[n][m] = PATH;//final dot
        return TRUE;
    }

    arr[n][m] = PATH;

    for(int i = 0; i < 4; i++)
        if(arr[n + k[i]][m + l[i]] == FREE)
            if(findPath(arr, n + k[i], m + l[i], arrN, arrM))
                return TRUE;

    arr[n][m] = BLOCK;
    return FALSE;
}

int main()
{
    int n = 0,
        m = 0,
        arrN, 
        arrM;

    char ** lab;

    FILE * fp = fopen("lab.txt", "rt");
    
    if(!fp)
    {
        perror("file: ");
        return -1;
    }
    
    sizeOfLab(fp, &arrN, &arrM);
    printf("%d X %d\n\n", arrN, arrM);
    
    lab = (char **)malloc(arrN * sizeof(char*));
    for(int i = 0; i < arrN; i++)
        lab[i] = (char *)malloc(arrM * sizeof(char));

    readLab(lab, fp, arrN, arrM);

    printLab(lab, arrN, arrM);
    findMan(lab, &n, &m);
    printf("n = %d\n"
           "m = %d\n\n", n, m);
    findPath(lab, n, m, arrN, arrM);
    printLab(lab, arrN, arrM);

    fclose(fp);

    for(int i = 0; i < arrN; i++)
        free(lab[i]);
    free(lab);

    printf("Done!\n\n");
    return 0;
}