//1. �������� ���������, ������� ����������� � ������������ ���,
//���� � ���, � ����� ����������� ����������� ����� � ����, ����-
//��� ������������ � ���������� ��������� (��������, ����������,
//�����)

#include <stdio.h>
#define MATURE_AGE 18
#define MIN_GROWTH 100
#define MAX_GROWTH 250
#define MIN_WEIGHT 30
#define MAX_WEIGHT 200
#define MAN 100
#define WOMAN 110
#define DELTA_WEIGHT 5

int main()
{
	int age, gender, growth, weight, ideal;
	do
	{
		printf("How many to you full years?  ");
		scanf("%d", &age);
		if (age >= MATURE_AGE)
		{
			printf("Please, enter your gender (1 - for men, 2 - for women): ");
			scanf("%d", &gender);
			if (gender==1 || gender==2)
				break;
		}
		else
		{
			printf("This test doesn't suit you! Your age < %d years.\n", MATURE_AGE);
			return 1;
		}
	}
	while(1);
	do
	{
		printf("Please, enter your growth in santimetrs: from %d to %d sm.: ", MIN_GROWTH, MAX_GROWTH);
		scanf("%d", &growth);
		if (growth>=MIN_GROWTH && growth<=MAX_GROWTH)
			break;
	}
	while(1);
	do
	{
		printf("Please, enter your weight in kilogramms: from %d to %d kg.: ", MIN_WEIGHT, MAX_WEIGHT);
		scanf("%d", &weight);
		if (weight>=MIN_WEIGHT && weight<=MAX_WEIGHT)
			break;
	}
	while(1);

	if(gender==1)
		ideal=growth-MAN;
	else
		ideal=growth-WOMAN;

	// recomedation
	if (weight < ideal - DELTA_WEIGHT)
		printf("You need to increase your weight (for your growth %d ideal weight is %d)\n", growth, ideal);
	else if( weight > ideal + DELTA_WEIGHT)
		printf("You need to decrease your weight (for your growth %d ideal weight is %d)\n", growth, ideal);
	else
		printf("You weight is normal (your ideal weight is %d)\n", ideal);

	return 0;
}
