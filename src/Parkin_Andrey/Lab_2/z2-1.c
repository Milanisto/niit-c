/*1. �������� ���������, ����������� ������ ���������� �����. 
����� ������ � ������ H, ������� �������� �������������. � �����
������ ������� ����� ������ ���������� ���������� �� �������
L =gt^2/2, ��� g = 9.81 mc^2.
��������� ����� ����������� ��� � ������� � ������� �� �����-
��� ������� �������� ������ ��� ������������ ����� h.
���������:
t=00 c h=5000.0 �
t=01 c h=4995.4 �
...
t=31� h=0234.8 �
BABAH!!!
*/
#include<windows.h>
#include<math.h>
#include<stdio.h>
#define G 9.81
#define SEC 1000
#define MAX_H 35000

int main()
{
	int i;
	float H=0, L=0, time=0;
	char timer[][10] = {"THREE", "TWO", "ONE", "START!!!"};
	while(1)
	{
		printf("Please, enter height in meters (from 1 to %d m): ", MAX_H);
		scanf("%f",&H);
		if (H>MAX_H || H<1)
			printf("Try again\n");
		else
			break;
	}
	for(i=0; i<=3; i++)
	{
		printf("%s  ", timer[i]);
		Sleep(SEC);
	}
	printf("\n");
	while(L<H)
	{
		L = (G*powf(time,2))/2;
		if(L<H)
		{
			printf("t=%.2f sec, h=%.1f meters\n", time, H-L);
			Sleep(SEC);
			time = time + 1;
		}
	}
	printf("\nBDISH!!! BOOH!!! BANG!!! BABAH!!!\n\n");
	return 0;
}
