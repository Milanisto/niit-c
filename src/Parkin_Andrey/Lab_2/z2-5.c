/*5. �������� ���������, ������� ������� �� ����� 10 �������, ���������������
��������� ������� �� ��������� ���� � ����, ������ ����� ������ ����
��� � ������, ��� � � ������� ���������. ����� ������ - 8 ��������.
���������:
������ ���������������� ������: Nh1ku83k*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define psw_num 10
#define psw_len 8

int main()
{ 
	int i, j;
	char psw_arr[psw_num][psw_len + 1];
	srand(time(NULL));
	for(i=0; i<psw_num; i++)
	{
		for(j=0;j<psw_len;j++)
		{
			switch (1 + rand()%3)
			{
			case 1:
				psw_arr[i][j] = 'a' + rand()%('z'-'a'+1);
				break;
			case 2:
				psw_arr[i][j] = 'A' + rand()%('Z'-'A'+1);
				break;
			case 3:
				psw_arr[i][j] = '0' + rand()%('9'-'0'+1);
				break;
			default:
				printf("ERROR!!!");
			}
		}
		psw_arr[i][j] = '\0';
	}
	for(i=0; i<psw_num; i++)
		printf("%2d : %s\n",i+1, psw_arr[i]);
	return 0;
}